
function printCars() {
    var doc = new jsPDF();
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " | "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

    var totalPagesExp = "{total_pages_count_string}";

    doc.setFontSize(9);
    doc.setTextColor(0);
    doc.setFontStyle('bold');
    var pageSize = doc.internal.pageSize;
    var pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    var text = doc.splitTextToSize("Data: " + datetime, pageWidth - 35, {});
    doc.text(text, 15, 60);

    doc.autoTable({
        styles:{
            valign: 'middle',
            halign : 'center'
          },
        // head: headRows(),
        html: '.table-print',
        didDrawPage: function (data) {
            // Header
            doc.setFontSize(20);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            if (headerImgData) {
                doc.addImage(headerImgData, 'png', data.settings.margin.left, 15, 60, 30);
            }
            doc.text("Lista de Autocarros no Sistema", data.settings.margin.left+80, 35);
            // Footer
            var str = "Pág. " + doc.internal.getNumberOfPages()
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                str = "Gerado Por Computador"+ " | " + str + " de " + totalPagesExp;
            }
            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
            doc.text(str, data.settings.margin.left, pageHeight - 10);
        },
        margin: {top: 70}
    });

    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
        doc.save("Lista de autocarros.pdf");
};



function printRoutes() {
    var doc = new jsPDF();
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " | "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

    var totalPagesExp = "{total_pages_count_string}";

    doc.setFontSize(9);
    doc.setTextColor(0);
    doc.setFontStyle('bold');
    var pageSize = doc.internal.pageSize;
    var pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    var text = doc.splitTextToSize("Data: " + datetime, pageWidth - 35, {});
    doc.text(text, 15, 60);

    doc.autoTable({
        styles:{
            valign: 'middle',
            halign : 'center',
          },
        // head: headRows(),
        html: '.table-print',
        didDrawPage: function (data) {
            // Header
            doc.setFontSize(20);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            if (headerImgData) {
                doc.addImage(headerImgData, 'png', data.settings.margin.left, 15, 60, 30);
            }
            doc.text("Lista de Rotas Registadas", data.settings.margin.left+80, 35);
            // Footer
            var str = "Pág. " + doc.internal.getNumberOfPages()
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                str = "Gerado Por Computador"+ " | " + str + " de " + totalPagesExp;
            }
            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
            doc.text(str, data.settings.margin.left, pageHeight - 10);
        },
        margin: {top: 70}
    });

    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
        doc.save("Lista de rotas.pdf");
};

function printStoppages() {
    var doc = new jsPDF();
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " | "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

    var totalPagesExp = "{total_pages_count_string}";

    doc.setFontSize(9);
    doc.setTextColor(0);
    doc.setFontStyle('bold');
    var pageSize = doc.internal.pageSize;
    var pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    var text = doc.splitTextToSize("Data: " + datetime, pageWidth - 35, {});
    doc.text(text, 15, 60);

    doc.autoTable({
        styles:{
            valign: 'middle',
            halign : 'center',
          },
        // head: headRows(),
        html: '.table-print',
        didDrawPage: function (data) {
            // Header
            doc.setFontSize(20);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            if (headerImgData) {
                doc.addImage(headerImgData, 'png', data.settings.margin.left, 15, 60, 30);
            }
            doc.text("Lista de Paragens", data.settings.margin.left+80, 35);
            // Footer
            var str = "Pág. " + doc.internal.getNumberOfPages()
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                str = "Gerado Por Computador"+ " | " + str + " de " + totalPagesExp;
            }
            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
            doc.text(str, data.settings.margin.left, pageHeight - 10);
        },
        margin: {top: 70}
    });

    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
        doc.save("Lista de paragens.pdf");
};

function printHours() {
    var doc = new jsPDF();
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " | "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

    var totalPagesExp = "{total_pages_count_string}";

    doc.setFontSize(9);
    doc.setTextColor(0);
    doc.setFontStyle('bold');
    var pageSize = doc.internal.pageSize;
    var pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    var text = doc.splitTextToSize("Data: " + datetime, pageWidth - 35, {});
    doc.text(text, 15, 60);

    doc.autoTable({
        styles:{
            valign: 'middle',
            halign : 'center',
          },
        // head: headRows(),
        html: '.table-print',
        didDrawPage: function (data) {
            // Header
            doc.setFontSize(20);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            if (headerImgData) {
                doc.addImage(headerImgData, 'png', data.settings.margin.left, 15, 60, 30);
            }
            doc.text("Lista de Horarios", data.settings.margin.left+80, 35);
            // Footer
            var str = "Pág. " + doc.internal.getNumberOfPages()
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                str = "Gerado Por Computador"+ " | " + str + " de " + totalPagesExp;
            }
            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
            doc.text(str, data.settings.margin.left, pageHeight - 10);
        },
        margin: {top: 70}
    });

    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
        doc.save("Lista de horarios.pdf");
};


function printBooks() {
    var doc = new jsPDF();
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " | "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

    var totalPagesExp = "{total_pages_count_string}";

    doc.setFontSize(9);
    doc.setTextColor(0);
    doc.setFontStyle('bold');
    var pageSize = doc.internal.pageSize;
    var pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    var text = doc.splitTextToSize("Data: " + datetime, pageWidth - 35, {});
    doc.text(text, 15, 60);

    doc.autoTable({
        styles:{
            valign: 'middle',
            halign : 'center',
          },
        // head: headRows(),
        html: '.table-print',
        didDrawPage: function (data) {
            // Header
            doc.setFontSize(20);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            if (headerImgData) {
                doc.addImage(headerImgData, 'png', data.settings.margin.left, 15, 60, 30);
            }
            doc.text("Lista de Reservas Registadas", data.settings.margin.left+80, 35);
            // Footer
            var str = "Pág. " + doc.internal.getNumberOfPages()
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                str = "Gerado Por Computador"+ " | " + str + " de " + totalPagesExp;
            }
            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
            doc.text(str, data.settings.margin.left, pageHeight - 10);
        },
        margin: {top: 70}
    });

    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
        doc.save("Lista de rotas.pdf");
};

var tTB = document.getElementById("tablesUsers");
var atTB = doc.autoTableHtmlToJson(tTB, true);
var cols = atTB.columns;


function printUsers() {
    var doc = new jsPDF();

    var tbl = $('#tablesUsers').clone();
    tbl.find('tr th:nth-child(2), tr td:nth-child(2)').remove();
    tbl.find('tr th:nth-child(8), tr td:nth-child(8)').remove();

    var res = doc.autoTableHtmlToJson(tbl.get(0));


    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " | "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

    var totalPagesExp = "{total_pages_count_string}";

    //here you are going to set which column you will truncate. Moreover, .splice(index number of the column(your start), the number of columns you will exclude)


    doc.setFontSize(9);
    doc.setTextColor(0);
    doc.setFontStyle('bold');
    var pageSize = doc.internal.pageSize;
    var pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    var text = doc.splitTextToSize("Data: " + datetime, pageWidth - 35, {});
    doc.text(text, 15, 60);

    doc.autoTable(res.columns, res.data,{
        styles:{
            valign: 'middle',
            halign : 'center',
          },
        // head: headRows(),
        html: '#tablesUsers',
        didDrawPage: function (data) {
            // Header
            doc.setFontSize(20);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            if (headerImgData) {
                doc.addImage(headerImgData, 'png', data.settings.margin.left, 15, 60, 30);
            }
            doc.text("Lista de Usuarios Registados", data.settings.margin.left+80, 35);
            // Footer
            var str = "Pág. " + doc.internal.getNumberOfPages()
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                str = "Gerado Por Computador"+ " | " + str + " de " + totalPagesExp;
            }
            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
            doc.text(str, data.settings.margin.left, pageHeight - 10);
        },
        margin: {top: 70}
    });

    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp);
    }
        doc.save("Lista de rotas.pdf");
};