<?php

use Illuminate\Database\Seeder;

use App\Role;

class RoleTableSeeder extends Seeder{

    public function run(){
        $role_superuser = new Role();
        $role_superuser->name = 'Superuser';
        $role_superuser->description = 'Administrador do sistema, usuario máximo do sistema';
        $role_superuser->save();

        $role_admin = new Role();
        $role_admin->name = 'Administrador';
        $role_admin->description = 'Administrador do sistema, similar ao administrador';
        $role_admin->save();
        
        $role_manager = new Role();
        $role_manager->name = 'Gestor';
        $role_manager->description = 'Apenas funcionalidades de gestão da plataforma';
        $role_manager->save();

        $role_driver = new Role();
        $role_driver->name = 'Condutor';
        $role_driver->description = 'Condutor de autocarros da empresa, verifica passagens e seu autocarro';
        $role_driver->save();

        $role_null = new Role();
        $role_null->name = 'Normal';
        $role_null->description = 'Este usuario nao possui nenhum cargo';
        $role_null->save();
    }
}
