<?php

use Illuminate\Database\Seeder;

use App\Route;
use App\Stoppage;
use App\RouteDayTime;

class RouteTableSeeder extends Seeder{

    public function run(){
        $routes = [
            ['bus_id'=>1,'from'=>1,'to'=>3,'price'=>800,'state'=>'Active'],
            ['bus_id'=>2,'from'=>1,'to'=>4,'price'=>1500,'state'=>'Active'],
            ['bus_id'=>3,'from'=>1,'to'=>6,'price'=>2400,'state'=>'Active'],
            ['bus_id'=>4,'from'=>1,'to'=>9,'price'=>3100,'state'=>'Active'],
            ['bus_id'=>5,'from'=>1,'to'=>10,'price'=>2400,'state'=>'Active'],
            ['bus_id'=>6,'from'=>1,'to'=>8,'price'=>3600,'state'=>'Active'],
            ['bus_id'=>7,'from'=>1,'to'=>7,'price'=>4200,'state'=>'Active']
        ];
       
        foreach($routes as $route){
            Route::create($route);
        }

        $stoppages = [
            ['route_id'=>1,'province_id'=>3,'stop_name'=>'Mercado Central, Av. da Revolução No. 234A'],
            ['route_id'=>2,'province_id'=>4,'stop_name'=>'Beira, Avenida do Bagamoyo (ao lado dos bombeiros)'],
            ['route_id'=>3,'province_id'=>6,'stop_name'=>'Parque Municipal'],
            ['route_id'=>4,'province_id'=>9,'stop_name'=>'Igreja São Jose'],
            ['route_id'=>5,'province_id'=>10,'stop_name'=>'Romosa'],
            ['route_id'=>6,'province_id'=>8,'stop_name'=>'Av. 25 de Setembro 575'],
            ['route_id'=>7,'province_id'=>7,'stop_name'=>'Mercado Central'],
        ];
       
        foreach($stoppages as $stoppage){
            Stoppage::create($stoppage);
        }

        $routeDaysTimes = [
            ['route_id'=>1,'route_day_id'=>1,'departure_time'=>'05:00:00'],
            ['route_id'=>1,'route_day_id'=>2,'departure_time'=>'05:00:00'],
            ['route_id'=>1,'route_day_id'=>3,'departure_time'=>'05:00:00'],
            ['route_id'=>1,'route_day_id'=>4,'departure_time'=>'05:00:00'],
            ['route_id'=>1,'route_day_id'=>5,'departure_time'=>'05:00:00'],
            ['route_id'=>1,'route_day_id'=>6,'departure_time'=>'05:00:00'],
            ['route_id'=>1,'route_day_id'=>7,'departure_time'=>'05:00:00'],

            ['route_id'=>2,'route_day_id'=>2,'departure_time'=>'15:00:00'],
            ['route_id'=>2,'route_day_id'=>4,'departure_time'=>'15:00:00'],

            ['route_id'=>3,'route_day_id'=>1,'departure_time'=>'05:00:00'],
            ['route_id'=>3,'route_day_id'=>4,'departure_time'=>'05:00:00'],

            ['route_id'=>4,'route_day_id'=>1,'departure_time'=>'04:00:00'],
            ['route_id'=>4,'route_day_id'=>4,'departure_time'=>'04:00:00'],

            ['route_id'=>5,'route_day_id'=>1,'departure_time'=>'04:00:00'],
            ['route_id'=>5,'route_day_id'=>3,'departure_time'=>'04:00:00'],
            ['route_id'=>5,'route_day_id'=>5,'departure_time'=>'04:00:00'],

            ['route_id'=>6,'route_day_id'=>1,'departure_time'=>'04:00:00'],
            ['route_id'=>6,'route_day_id'=>4,'departure_time'=>'04:00:00'],

            ['route_id'=>7,'route_day_id'=>3,'departure_time'=>'04:00:00'],
            ['route_id'=>7,'route_day_id'=>4,'departure_time'=>'04:00:00'],
        ];
       
        foreach($routeDaysTimes as $routeDay){
            RouteDayTime::create($routeDay);
        }
    }
}
