<?php

use Illuminate\Database\Seeder;

class RouteDaysTableSeeder extends Seeder{

    public function run(){
        $routes = [
            ['route_day'=> 'Segunda-Feira'],
            ['route_day'=> 'Terça-Feira'],
            ['route_day'=> 'Quarta-Feira'],
            ['route_day'=> 'Quinta-Feira'],
            ['route_day'=> 'Sexta-Feira'],
            ['route_day'=> 'Sábado'],
            ['route_day'=> 'Domingo'],
        ];
        DB::table('route_days')->insert($routes);
    }
}
