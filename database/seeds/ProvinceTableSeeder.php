<?php

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder{
    
    public function run(){
        $provinces = [
            [ 'name' => 'Maputo'],
            [ 'name' => 'Gaza'],
            [ 'name' => 'Inhambane'],
            [ 'name' => 'Sofala'],
            [ 'name' => 'Manica'],
            [ 'name' => 'Tete'],
            [ 'name' => 'Niassa'],
            [ 'name' => 'Cabo Delgado'],
            [ 'name' => 'Nampula'],
            [ 'name' => 'Zambezia'],
        ];

        DB::table('provinces')->insert($provinces);
    }
}
