<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder{

    public function run(){
        $this->call(RoleTableSeeder::class);
        $this->call(ProvinceTableSeeder::class);
        $this->call(RouteDaysTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CarTableSeeder::class);
        $this->call(RouteTableSeeder::class);
    }
}
