<?php

use Illuminate\Database\Seeder;

use App\Bus;

class CarTableSeeder extends Seeder{

    public function run(){
        $cars = [
            ['reference'=>'LCK6120A','photo'=>'noCar.jpg', 'brand'=>'Zhongtong Bus', 'model'=>'Express', 'license_plate'=>'ADC 123 MP', 'seat_number'=>72, 'register_date'=>'2018-02-05', 'fuel_type'=>'Diesel', 'transmission'=>'Manual', 'bus_state'=>'Disponivel'],
            ['reference'=>'LCK6130A','photo'=>'noCar.jpg', 'brand'=>'Zhongtong Bus', 'model'=>'Tourism', 'license_plate'=>'AIC 234 MC', 'seat_number'=>72, 'register_date'=>'2016-02-05', 'fuel_type'=>'Diesel', 'transmission'=>'Automático', 'bus_state'=>'Disponivel'],
            ['reference'=>'LCK6140A','photo'=>'noCar.jpg', 'brand'=>'Zhongtong Bus', 'model'=>'Tourism', 'license_plate'=>'AFC 453 MC', 'seat_number'=>72, 'register_date'=>'2017-06-11', 'fuel_type'=>'Diesel', 'transmission'=>'Manual', 'bus_state'=>'Disponivel'],
            ['reference'=>'LCK6150A','photo'=>'noCar.jpg', 'brand'=>'Zhongtong Bus', 'model'=>'Tourism', 'license_plate'=>'AEC 342 MP', 'seat_number'=>72, 'register_date'=>'2019-02-05', 'fuel_type'=>'Diesel', 'transmission'=>'Automático', 'bus_state'=>'Indisponivel'],
            ['reference'=>'LCK6160A','photo'=>'noCar.jpg', 'brand'=>'Zhongtong Bus', 'model'=>'Tourism', 'license_plate'=>'AAC 343 MC', 'seat_number'=>72, 'register_date'=>'2015-03-25', 'fuel_type'=>'Diesel', 'transmission'=>'Manual', 'bus_state'=>'Disponivel'],
            ['reference'=>'LCK6170A','photo'=>'noCar.jpg', 'brand'=>'Zhongtong Bus', 'model'=>'Tourism', 'license_plate'=>'AHC 322 MP', 'seat_number'=>72, 'register_date'=>'2016-04-05', 'fuel_type'=>'Diesel', 'transmission'=>'Automático', 'bus_state'=>'Disponivel'],
            ['reference'=>'LCK6180A','photo'=>'noCar.jpg', 'brand'=>'Zhongtong Bus', 'model'=>'Tourism', 'license_plate'=>'AFE 645 MP', 'seat_number'=>72, 'register_date'=>'2017-07-15', 'fuel_type'=>'Diesel', 'transmission'=>'Automático', 'bus_state'=>'Disponivel']
        ];
        
        foreach($cars as $car){
            Bus::create($car);
        }
    }
}
