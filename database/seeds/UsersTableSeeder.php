<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;
use App\UserDetail;

class UsersTableSeeder extends Seeder{

    public function run(){

        $role_admin         = Role::where('name', 'Administrador')->first();
        $role_manager       = Role::where('name', 'Gestor')->first();
        $role_driver        = Role::where('name', 'Condutor')->first();
        $role_superuser     = Role::where('name', 'Superuser')->first();

        $admin = new User();
        $admin->name = 'Mouzinho dos Santos Zacarias';
        $admin->username = 'mouzinho';
        $admin->email = 'mouzinho388@gmail.com';
        $admin->password = bcrypt('12345678');
        $admin->save();
        $admin->roles()->attach($role_admin);

        $adminDetail = new UserDetail();
        $adminDetail->user_id = $admin->id;
        $adminDetail->profile_photo = 'noProfile.png';
        $adminDetail->save();

        $superuser = new User();
        $superuser->name = 'Mercidio Huo';
        $superuser->username = 'mercidio';
        $superuser->email = 'mercidio@gmail.com';
        $superuser->password = bcrypt('12345678');
        $superuser->save();
        $superuser->roles()->attach($role_superuser);

        $superuserDetail = new UserDetail();
        $superuserDetail->user_id = $superuser->id;
        $superuserDetail->profile_photo = 'noProfile.png';
        $superuserDetail->save();

        $manager = new User();
        $manager->name = 'Isabel Anastacio';
        $manager->username = 'isabel';
        $manager->email = 'isabel@gmail.com';
        $manager->password = bcrypt('12345678');
        $manager->save();
        $manager->roles()->attach($role_manager);

        $managerDetail = new UserDetail();
        $managerDetail->user_id = $manager->id;
        $managerDetail->profile_photo = 'noProfile.png';
        $managerDetail->save();

        $driver = new User();
        $driver->name = 'Manuel Xirindza';
        $driver->username = 'manuel';
        $driver->email = 'manuel@gmail.com';
        $driver->password = bcrypt('12345678');
        $driver->save();
        $driver->roles()->attach($role_driver);

        $driverDetail = new UserDetail();
        $driverDetail->user_id = $driver->id;
        $driverDetail->profile_photo = 'noProfile.png';
        $driverDetail->save();

    }
}
