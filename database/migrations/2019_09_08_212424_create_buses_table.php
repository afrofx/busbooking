<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusesTable extends Migration{
    
    public function up(){
        Schema::create('buses', function (Blueprint $table) {
            $table->Increments('bus_id');
            $table->string('photo');
            $table->string('reference')->unique();
            $table->string('brand');
            $table->string('model');
            $table->string('license_plate');
            $table->integer('seat_number')->default(75);
            $table->date('register_date');
            $table->enum('fuel_type', ['Gasolina', 'Diesel', 'Hibrido']);
            $table->enum('transmission', ['Automatico', 'Manual']);
            $table->enum('bus_state', ['Disponivel', 'Indisponivel']);
            $table->softDeletes();
            $table->timestamps();
        });
    }
    
    public function down(){
        Schema::dropIfExists('buses');
    }
}
