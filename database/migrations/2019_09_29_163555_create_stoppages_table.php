<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoppagesTable extends Migration{

    public function up(){
        Schema::create('stoppages', function (Blueprint $table) {
            $table->bigIncrements('stoppage_id');
            $table->unsignedInteger('province_id');
            $table->unsignedInteger('route_id');
            $table->string('stop_name');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('route_id')->references('route_id')->on('routes');
            $table->foreign('province_id')->references('province_id')->on('provinces');
        });
    }

    public function down(){
        Schema::dropIfExists('stoppages');
    }
}
