<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteDayTimesTable extends Migration{

    public function up(){
        Schema::create('route_day_times', function (Blueprint $table) {
            $table->Increments('day_time_id');
            $table->unsignedInteger("route_day_id");
            $table->unsignedInteger('route_id');
            $table->time('departure_time');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('route_day_id')->references('route_day_id')->on('route_days');
            $table->foreign('route_id')->references('route_id')->on('routes');
        });
    }

    public function down(){
        Schema::dropIfExists('route_day_times');
    }
}
