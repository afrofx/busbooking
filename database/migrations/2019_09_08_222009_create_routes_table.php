<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration{

    public function up(){
        Schema::create('routes', function (Blueprint $table) {
            $table->Increments('route_id');
            $table->unsignedInteger('bus_id');
            $table->unsignedInteger('from');
            $table->unsignedInteger('to');
            $table->decimal('price', 8, 2);;
            $table->enum('state',['Active','Inactive']);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('bus_id')->references('bus_id')->on('buses');
            $table->foreign('from')->references('province_id')->on('provinces');
            $table->foreign('to')->references('province_id')->on('provinces');
        });
    }
    
    public function down(){
        Schema::dropIfExists('routes');
    }
}
