<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration{
    
    public function up(){
        Schema::create('user_details', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedBigInteger('user_id');
            $table->string('id_number')->unique()->nullable();
            $table->string('profile_photo')->default('noProfile.png');
            $table->string('birthday')->nullable();
            $table->string('address')->nullable();
            $table->enum('marital_status', ['Solteiro', 'Casado'])->nullable();
            $table->enum('gender', ['Femenino', 'Masculino'])->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down(){
        Schema::dropIfExists('user_details');
    }
}
