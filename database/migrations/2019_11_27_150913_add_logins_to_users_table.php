<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoginsToUsersTable extends Migration{
    public function up(){
        Schema::table('users', function (Blueprint $table) { 
            $table->datetime('last_login_at')->nullable(); 
            $table->text('last_login_ip')->nullable(); 
        });
    }

    public function down(){
        Schema::table('users', function (Blueprint $table) { 
            $table->dropColumn('last_login_at')->nullable(); 
            $table->dropColumn('last_login_ip')->nullable(); 
        });
    }
}
