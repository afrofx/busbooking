<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteDaysTable extends Migration{

    public function up(){
        Schema::create('route_days', function (Blueprint $table) {
            $table->Increments('route_day_id');
            $table->string('route_day');
            $table->softDeletes();
        });
    }

    public function down(){
        Schema::dropIfExists('route_days');
    }
}
