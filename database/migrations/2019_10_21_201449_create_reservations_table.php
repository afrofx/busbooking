<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration{

    public function up(){
        Schema::create('reservations', function (Blueprint $table) {
            $table->Increments('reservation_id');
            $table->unsignedInteger("route_id");
            $table->string("reference")->unique;
            $table->string("name");
            $table->string("surname");
            $table->date("birthday");
            $table->string("email");
            $table->string("contact");
            $table->string("id_number");
            $table->date("departure_date");
            $table->date("return_date");
            $table->enum("book_state",['Active','Canceled','Paid']);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('route_id')->references('route_id')->on('routes');
        });
    }
    

    public function down(){
        Schema::dropIfExists('reservations');
    }
}
