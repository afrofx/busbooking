<?php

namespace App\Providers;

use App\User;
use App\Policies\UsersPolicy;
use App\Operator;
use App\Policies\OperatorsPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UsersPolicy::class,
        Operator::class => OperatorsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('user-control', 'App\Policies\UsersPolicy');
        Gate::define('operator-control', 'App\Policies\OperatorsPolicy');
    }
}
