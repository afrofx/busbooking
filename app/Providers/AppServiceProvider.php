<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;

class AppServiceProvider extends ServiceProvider{
    
    
    public function register(){
       
    }
    
    public function boot(){

        Schema::defaultStringLength(191);

        Route::resourceVerbs([
            'create' => 'adicionar',
            'edit' => 'editar',
        ]);
    }

    
}
