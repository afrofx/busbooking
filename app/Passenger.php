<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model{
    protected $fillable = [
        'province_id',
        'address',
        'name',
        'contact',
        'email',
        'parent',
    ];

    protected $primaryKey = 'passenger_id';

    protected $table = 'passengers';
}
