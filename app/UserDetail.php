<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetail extends Model{

    use SoftDeletes;
    
    protected $fillable = [
        'user_id',
        'gender',
        'birthday',
        'marital_status',
        'profile_photo',
        'id_number',
        'address',
        'about_me',
        'contact'
    ];

    protected $primaryKey = 'id';

    protected $table = 'user_details';

    public function user(){
        return $this->belongsTo(User::class);
    }
}
