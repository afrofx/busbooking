<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RouteDays extends Model{
    use SoftDeletes;

    protected $fillable = [
        'route_day',
    ];

    protected $primaryKey = 'route_day_id';

    protected $table = 'route_days';
}
