<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stoppage extends Model{
    use SoftDeletes;
    protected $fillable = [
        'route_id',
        'province_id',
        'stop_name'
    ];

    public function route(){
        return $this->belongsTo(Route::class, 'route_id');
    }

    public function province(){
        return $this->belongsTo(Province::class, 'province_id');
    }

    protected $primaryKey = 'stoppage_id';

    protected $table = 'stoppages';
}
