<?php

namespace App\Policies;

use App\User;
use App\Operator;
use Illuminate\Auth\Access\HandlesAuthorization;

class OperatorsPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability){
        if ($user->roles === 'administrador') {
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * Determine whether the user can view any operators.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the operator.
     *
     * @param  \App\User  $user
     * @param  \App\Operator  $operator
     * @return mixed
     */
    public function view(User $user, Operator $operator)
    {
        //
    }

    /**
     * Determine whether the user can create operators.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the operator.
     *
     * @param  \App\User  $user
     * @param  \App\Operator  $operator
     * @return mixed
     */
    public function update(User $user, Operator $operator)
    {
        //
    }

    /**
     * Determine whether the user can delete the operator.
     *
     * @param  \App\User  $user
     * @param  \App\Operator  $operator
     * @return mixed
     */
    public function delete(User $user, Operator $operator)
    {
        //
    }

    /**
     * Determine whether the user can restore the operator.
     *
     * @param  \App\User  $user
     * @param  \App\Operator  $operator
     * @return mixed
     */
    public function restore(User $user, Operator $operator)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the operator.
     *
     * @param  \App\User  $user
     * @param  \App\Operator  $operator
     * @return mixed
     */
    public function forceDelete(User $user, Operator $operator)
    {
        //
    }
}
