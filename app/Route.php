<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Route extends Model{
    use SoftDeletes;
    protected $fillable = [
        'bus_id',
        'stop_start',
        'price',
        'province_id',
        'stoppage',
        'time'
    ];

    public function buses(){
        return $this->belongsTo(Bus::class, 'bus_id');
    }

    public function stoppages(){
        return $this->hasMany(Stoppage::class, 'stoppage_id');
    }

    public function routeDayTime(){
        return $this->hasMany(RouteDayTime::class, 'day_time_id');
    }

    protected $primaryKey = 'route_id';

    protected $table = 'routes';
}
