<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable{
    
    use Notifiable, SoftDeletes;
    
    protected $fillable = [
        'name', 
        'email', 
        'username', 
        'password',
        'last_login_at',
        'last_login_ip',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
      return $this->belongsToMany(Role::class);
    }

    public function authorizeRoles($roles){
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) || 
            abort(401, 'Funcionalidade indisponivel contace o administrador.');
        }
        return $this->hasRole($roles) || 
        abort(401, 'Funcionalidade indisponivel contace o administrador.');
    }

    public function hasAnyRole($roles){
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    
    public function hasRole($role){
        return null !== $this->roles()->where('name', $role)->first();
    }

    public function userDetails(){
        return $this->hasOne(UserDetail::class);
    }
}
