<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bus extends Model{

    use SoftDeletes;
    
    protected $fillable = [
        'reference',
        'photo',
        'model',
        'register_date',
        'license_plate',
        'brand',
        'seat_number',
        'transmission',
        'fuel_type',
        'state'
    ];

    public function buses(){
        return $this->hasMany(Route::class);
    }

    protected $primaryKey = 'bus_id';

    protected $table = 'buses';

    protected $dates = ['created_at'];
}
