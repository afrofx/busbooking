<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RouteDayTime extends Model{
    use SoftDeletes;

    protected $fillable = [
        'route_id',
        'route_day_id',
        'departure_time'
    ];

    public function route_day(){
        return $this->belongsTo(Role::class);
    }

    public function route(){
        return $this->belongsTo(Route::class, 'route_id');
    }


    protected $primaryKey = 'day_time_id';

    protected $table = 'route_day_times';
}
