<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model{
    protected $fillable = [
        'name',
    ];

    public function stoppages(){
        return $this->hasMany(Stoppage::class);
    }

    protected $primaryKey = 'province_id';

    protected $table = 'provinces';
}
