<?php

namespace App\Http\Controllers;

use App\Bus;
use Illuminate\Http\Request;

use App\Http\Requests\DestinyRequest;
use App\Http\Requests\HoursRequest;
use App\Http\Requests\StoppageRequest;
use App\Province;
use App\Route;
use App\RouteDays;
use App\RouteDayTime;
use App\Stoppage;
use Illuminate\Support\Facades\DB;

class DestinyController extends Controller{

    public function __construct(){
        $this->middleware('auth')->except(['findDestinyList', 'Details']);
    }

    

    public function Details(){
        $routes = Route::with('buses')->get();
        $provinces  = Province::all();
        // return view('modules.routes.index', compact('routes', 'provinces'));
    }
    
    


    public function routes(){
        $buses  = Bus::all();
        $routes = Route::with('buses')->get();
        $provinces  = Province::all();
        return view('modules.routes.routes', compact('routes', 'provinces', 'buses'));
    }

    public function routesSave(DestinyRequest $request){
        DB::table('routes')->insert([
            'bus_id'        => $request->get('car'),
            'from'          => $request->get('departure'),
            'to'            => $request->get('arrival'),
            'price'         => $request->get('ticket_price'),
            'state'         => $request->get('route_state'),
            "created_at"    => \Carbon\Carbon::now(),
            "updated_at"    => \Carbon\Carbon::now(), 
            ]
        );
        return redirect()->route('rotas.index')->with('success', "Rota Adicionada Com Sucesso");
    }

    public function routesRemove($id){
        $route = Route::find($id);
        $route->delete();
        return redirect()->route('rotas.index')->with('success', "Rota Removida");
    }

    public function stoppages(){
        $stoppages = Stoppage::with('province', 'route')->get();
        $provinces  = Province::all();
        $routes = Route::with('buses')->get();
        return view('modules.routes.stoppages', compact('stoppages', 'provinces', 'routes'));
    }

    public function stoppagesSave(Request $request){
        DB::table('stoppages')->insert([
            'route_id'      => $request->get('stoppage_route'),
            'province_id'   => $request->get('stoppage_province'),
            'stop_name'     => $request->get('stoppage_name'),
            ]
        );
        return redirect()->route('rotas.paragens')->with('success', "Paragem Adicionada Com Sucesso");
    }

    public function stoppagesRemove($id){
        $stoppage = Stoppage::find($id);
        $stoppage->delete();
        return redirect()->route('rotas.paragens')->with('success', "Paragem Removida Removida");
    }

    public function hours(){
        $hours = RouteDayTime::with('route')->get();
        $route_days = RouteDays::all();
        $provinces  = Province::all();
        $routes = Route::with('buses')->get();
        return view('modules.routes.hours', compact('hours', 'provinces', 'route_days', 'routes'));
    }

    public function hoursSave(Request $request){
        DB::table('route_day_times')->insert([
            'route_id'          => $request->get('departure_route'),
            'route_day_id'      => $request->get('departure_day'),
            'departure_time'    => $request->get('departure_time'),
            ]
        );
        return redirect()->route('rotas.horarios')->with('success', "Horario Adicionado");
    }

    public function hoursRemove($id){
        $hour = RouteDayTime::find($id);
        $hour->delete();
        return redirect()->route('rotas.horarios')->with('success', "Horario Removido");
    }
}
