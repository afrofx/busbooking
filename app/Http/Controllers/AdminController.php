<?php

namespace App\Http\Controllers;

use App\Bus;
use App\Reservation;
use App\Route;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller{

    public function __construct(){
        $this->middleware(['auth']);
    }

    public function index(Request $request){
        $userId         = Auth::user()->id;
        $user           = User::where('id', $userId)->with('roles')->first();
        $users          = User::orderBy('last_login_at', 'DESC')->get();
        $user_details   = UserDetail::where('user_id', $userId)->first();
        $books          = Reservation::all()->count();
        $buses          = Bus::all()->count();
        $routes         = Route::all()->count();
        return view('home', compact('user', 'user_details', 'users', 'books', 'buses', 'routes'));
    }

}
