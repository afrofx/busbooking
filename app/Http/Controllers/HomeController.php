<?php

namespace App\Http\Controllers;

use App\Bus;
use Illuminate\Http\Request;

use DB;
use Session;
use App\User;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\BookingRequest;
use App\Province;
use App\Reservation;
use App\Route;
use App\UserDetail;

class HomeController extends Controller{

    public function __construct(){
    }

    public function index(){
        $routes = Route::with('buses')->get();
        $provinces  = Province::all();
        return view('inicio', compact('provinces','routes'));
    }

    public function about(){
        return view('acerca');
    }

    public function DestinyList(){
        $routes = Route::with('buses')->get();
        $provinces  = Province::all();
        return view('modules.routes.destinos', compact('provinces','routes'));
    }
    
    public function findDestinyList(Request $request){
        $data = DB::table('provinces')->where('province_id','!=', $request->province_id)->get();
        return response()->json($data);
    }
    
    public function showBook(){
        return view('modules.booking.reservation');
    }

    public function saveBook(){
        $value      = session('book_data');

        $destino    = DB::table('provinces')
                    ->where([['province_id','=',$value['arrival']]])
                    ->get();

        $partida    = DB::table('provinces')
                    ->where([['province_id','=',$value['departure']]])
                    ->get();

        $preco      = DB::table('routes')
                    ->where([['from','=',$value['departure']], ['to','=',$value['arrival']]])
                    ->get();

        $reservations = Reservation::where('route_id',$preco[0]->route_id )->where('book_state', "Active")->get();
                    
        return view('modules.booking.book', compact('value', 'destino', 'partida', 'preco', 'reservations'));
    }

    public function searchBook(BookingRequest $request){
        Session::put('book_data',[
            'departure'=>$request->departure,
            'arrival'=>$request->arrival,
            'departure_date'=>$request->departure_date,
            'book_type'=>$request->book_type,
            'return_date'=>$request->return_date,
            'passenger_numb'=>$request->passenger_numb,
        ]);
        
        $value = Session::get('book_data')['departure_date'];
        $from = Session::get('book_data')['departure'];
        $to = Session::get('book_data')['arrival'];
        $days = array('Domingo', 'Segunda-Feira', 'Terça-Feira', 'Quarta-Feira','Quinta-Feira','Sexta-Feira', 'Sabado');
        $day =  date('w', strtotime($value));
        $day_week = $days[$day];

        $routes = DB::table('routes')
                    ->join('route_day_times', 'routes.route_id', '=', 'route_day_times.route_id')
                    ->join('stoppages', 'routes.route_id', '=', 'stoppages.route_id')
                    ->join('route_days', 'route_day_times.route_day_id', '=', 'route_days.route_day_id')
                    ->where([['routes.from','=',$from], ['routes.to','=',$to]])
                    ->get();

        $provinces  = DB::table('provinces')->get();

        return view('modules.booking.search', compact('routes', 'provinces','day_week', 'value' ));

        $data = DB::table('provinces')->where('province_id','!=', $request->province_id)->get();
        return view('home');
    }

    public function seeReservation(Request $request, Reservation $reservation){
        $results = DB::table('reservations')
            ->select(DB::raw("*"))
            ->join('routes', 'reservations.route_id', '=', 'routes.route_id')
            ->where('reference', '=', $request->get('reference'))
            ->get();
        $provinces  =   DB::table('provinces')->get();
        
    	return view('modules.booking.mostrar', compact('results','provinces'));
    }

    public function storePassenger(Request $request){

       
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $reference = "";
        for ($i = 0; $i < 6; $i++) {
            $reference .= $chars[mt_rand(0, strlen($chars)-1)];
        }

        $value2     = session('book_data');
        $preco2      = DB::table('routes')
                    ->where([['from','=',$value2['departure']], ['to','=',$value2['arrival']]])
                    ->get();

        

        DB::table('reservations')->insert(
            [
                'reference'         => $reference,
                'route_id'          => $preco2[0]->route_id,
                'name'              => $request->get('name'),
                'surname'           => $request->get('surname'),
                'birthday'          => $request->get('birthday'),
                'contact'           => $request->get('contact'),
                'seat'              => $request->get('seat'),
                'parent'            => $request->get('parent'),
                'id_number'         => $request->get('id-number'),
                'email'             => $request->get('email'),
                'departure_date'    => $value2['departure_date'],
                'return_date'       => $value2['departure_date'],
                'book_state'        => 'Active',
                "created_at"        => \Carbon\Carbon::now(), # new \Datetime()
                "updated_at"        => \Carbon\Carbon::now(),  # new \Datetime()
                ]
        );

        $results = DB::table('reservations')
            ->select(DB::raw("*"))
            ->join('routes', 'reservations.route_id', '=', 'routes.route_id')
            ->where('reference', '=', $reference)
            ->get();
        $provinces  =   DB::table('provinces')->get();

        return view('modules.booking.mostrar', compact('results','provinces'));
    }
}
