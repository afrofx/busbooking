<?php

namespace App\Http\Controllers;

use App\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller{

    public function __construct(){
        $this->middleware(['auth']);
    }

    public function index(){
        $reservations = DB::table('reservations')->get();
        return view('modules.booking.index', compact('reservations'));
    }

    public function create(){
        
    }

    public function store(Request $request){

    }

    public function cancelBooking($id){
        
    }

    public function edit($id){
        return view('modules.booking.edit', compact(''));
    }
    
    public function update(Request $request, $id){
        
    }

    public function destroy($id){
        
    }
}
