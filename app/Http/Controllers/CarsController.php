<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CarRequest;
use App\Bus;
use DB;

class CarsController extends Controller{

    public function __construct(){
        // $this->middleware(['auth','admin'])->except(['index', 'show', 'showAuctionCategory']);
    }
    
    public function index(){
        $buses = Bus::all();
        return view('modules.cars.index', compact('buses'));
    }
    
    public function create(){
        return view('modules.cars.create');
    }


    public function store(CarRequest $request){

        if($request->hasFile('car_photo')){
            $fileNameWithExt =  $request->file('car_photo')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('car_photo')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
        }else{
            $fileNameToStore = 'noImage.jpg';
        }


        DB::table('buses')->insert([
            'photo'         => $fileNameToStore,
            'reference'     => $request->get('reference'),
            'brand'         => $request->get('car_brand'),
            'model'         => $request->get('car_model'),
            'license_plate' => $request->get('license_plate'),
            'seat_number'   => $request->get('car_seat_number'),
            'fuel_type'     => $request->get('car_fuel_type'),
            'transmission'  => $request->get('car_transmission'),
            'bus_state'     => $request->get('car_state'),
            'register_date' => $request->get('data-registo'),
            "created_at"    => \Carbon\Carbon::now(), # new \Datetime()
            "updated_at"    => \Carbon\Carbon::now(),  # new \Datetime()
        ]);

        $path = $request->file('car_photo')->storeAs('public/images/cars', $fileNameToStore);

        return redirect()->route('autocarros.index')->with('success', "Autocarro Registado");
    }
    

    public function show($id){
        $bus = Bus::find($id);
        return view('modules.cars.show', compact('bus'));
    }
    

    public function edit($id){
        $bus = Bus::find($id);
        return view('modules.cars.edit', compact('bus'));
    }
    
    public function update(Request $request, $id){
        $bus =  Bus::find($id);
        
        if($request->hasFile('car_photo')){
            $fileNameWithExt =  $request->file('car_photo')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('car_photo')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $bus->photo             =   $fileNameToStore;
            $path = $request->file('car_photo')->storeAs('public/images/cars', $fileNameToStore);
        }
        $bus->reference         =   $request->reference;
        $bus->brand             =   $request->car_brand;
        $bus->model             =   $request->car_model;
        $bus->license_plate     =   $request->license_plate;
        $bus->seat_number       =   $request->car_seat_number;
        $bus->fuel_type         =   $request->car_fuel_type;
        $bus->transmission      =   $request->car_transmission;
        $bus->bus_state         =   $request->car_state;
        $bus->register_date     =   $request->data_registo;
        $bus->updated_at        =   \Carbon\Carbon::now();
        $bus->save();
        return redirect()->route('autocarros.index')->with('success', "Autocarro Actualizado");
    }

    public function destroy($id){
        $bus =  Bus::find($id);
        $bus->delete();
        return redirect()->route('autocarros.index')->with('success', "Autocarro Removido");
    }
}
