<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


use App\Http\Requests\UsersRequest;
use App\Role;
use App\RoleUser;
use App\UserDetail;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;

class DriversController extends Controller{

    public function __construct(){
        // $this->authorizeResource(Operator::class, 'operator-control');
    }

    public function index(){
        $userId = Auth::user()->id;
        $users = User::with('roles')->get();
        $user_details = UserDetail::all();

        // return $operators;
        return view('modules.drivers.index', compact('users', 'user_details'));
    }

    
    public function create(){
        $roles = DB::table('roles')->get();
        return view('modules.drivers.create', compact('roles'));
    }


    public function store(UsersRequest $request){

        if($request->hasFile('op_photo')){
            $fileNameWithExt =  $request->file('op_photo')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('op_photo')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $path = $request->file('op_photo')->storeAs('public/images/users', $fileNameToStore);
        }else{
            $fileNameToStore = 'noProfile.jpg';
        }

        $user = User::create([
            'name'          => $request->name,
            'email'         => $request->email,
            'username'      => $request->username,
            'password'      => Hash::make($request->password),
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);

        $userDetail = UserDetail::create([
            'user_id'       => $user->id,
            'id_number'     => $request->id_number,
            'profile_photo' => $fileNameToStore,
            'birthday'      => $request->birthday,
            'address'       => $request->address,
            'contact'       => $request->contact,
            'about_me'      => $request->about,
            'marital_status'=> $request->m_status,
            'gender'        => $request->gender,
        ]);

        DB::table('role_user')->insert(
            [
                'user_id'       => $user->id,
                'role_id'       => $request->role,
            ]
        );

        
        return redirect()->route('usuarios.index')->with('success', "Usuario Adicionado");
    }
    

    public function show($id){
        $user =  User::with('roles')->find($id);
        $user_details = UserDetail::where('user_id', $user->id)->get();
        return view('modules.drivers.show', compact('user','user_details'));
    }
    
    public function UserProfile(){
        $userId = Auth::user()->id;
        $user = User::where('id', $userId)->with('roles')->first();
        $user_details = UserDetail::where('user_id', $userId)->first();
        return view('modules.drivers.profile', compact('user','user_details'));
    }

    public function edit($id){
        $user =  User::with('roles')->find($id);
        $roles = Role::all();
        $user_details = UserDetail::where('user_id', $user->id)->get();
        return view('modules.drivers.edit', compact('user', 'roles', 'user_details'));
    }
    
    public function update(Request $request, $id){
        $user =  User::find($id);
       
        $user->name = $request->name;
        $user->email = $request->email;
        $user->username =$request->username;
        $user->updated_at =  \Carbon\Carbon::now();
        if(!isset($request->password) || $request->password != null){
            $user->password =Hash::make($request->password);
        }
        $user->save();
        
        $user_details = UserDetail::where('user_id', $user->id)->get();
        if($request->hasFile('op_photo')){
            $fileNameWithExt =  $request->file('op_photo')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('op_photo')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $user_details[0]->profile_photo = $fileNameToStore;
            $path = $request->file('op_photo')->storeAs('public/images/users', $fileNameToStore);
        }

        $user_details[0]->id_number     = $request->id_number;
        $user_details[0]->birthday      = $request->birthday;
        $user_details[0]->address       = $request->address;
        $user_details[0]->contact       = $request->contact;
        $user_details[0]->about_me      = $request->about;
        $user_details[0]->marital_status= $request->m_status;
        $user_details[0]->gender        = $request->gender;
        $user_details[0]->updated_at    = \Carbon\Carbon::now();
        $user_details[0]->save();

        $role =RoleUser::where('user_id', $user->id)->get();
        $role[0]->role_id = $request->role;
        $role[0]->save();
        
        return redirect()->route('usuarios.index')->with('success', "Usuario Actualizado");
    }

    public function destroy($id){
        $user =  User::find($id);
        $user->delete();
        return redirect()->route('usuarios.index')->with('success', "Usuario Removido");
    }

    public function updateUser(Request $request){
        $user = User::find(Auth::user()->id);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->username = $request->username;
        if($request->password === $request->confirm_password){
            $user->password = bcrypt($request->password);
        }
        $user->save();
        return redirect()->route('usuarios.profile')->with('success', "Detalhes do usuario actualizados");
    }

    public function updateUserData(Request $request, $id){
        $user = User::find($id);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->username = $request->username;
        if($request->password != null && !isEmpty($request->password)){
            if($request->password === $request->confirm_password){
                $user->password = bcrypt($request->password);
            }else{
                return;
            }
        }
        $user->save();
        $user_details = UserDetail::where('user_id', $user->id)->get();
        return redirect()->route('usuarios.show', compact('user','user_details'))->with('success', "Detalhes do usuario actualizados");
    }
    

    public function updateUserInfo(Request $request){
        $user_details = UserDetail::find(Auth::user()->id);
        if($request->hasFile('image')){
            $fileNameWithExt =  $request->file('image')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $user_details->profile_photo    = $fileNameToStore;
            $path = $request->file('image')->storeAs('public/images/users', $fileNameToStore);
        }else{
            $fileNameToStore = 'noProfile.png';
        }
        $user_details->id_number        = $request->identity;
        $user_details->address          = $request->address;
        $user_details->contact          = $request->contact;
        $user_details->gender           = $request->gender;
        $user_details->marital_status   = $request->e_civil;
        $user_details->about_me         = $request->about;
        $user_details->birthday         = $request->birthday;
        $user_details->save();
        
        return redirect()->route('usuarios.profile')->with('success', "Detalhes do usuario actualizados");
    }

    public function updateDetails(Request $request, $id){
        $user = User::find($id);
        $user_details = UserDetail::where('user_id', $user->id)->get();
        if($request->hasFile('image')){
            $fileNameWithExt =  $request->file('image')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $user_details[0]->profile_photo    = $fileNameToStore;
            $path = $request->file('image')->storeAs('public/images/users', $fileNameToStore);
        }else{
            $fileNameToStore = 'noProfile.png';
        }
        $user_details[0]->id_number        = $request->identity;
        $user_details[0]->address          = $request->address;
        $user_details[0]->contact          = $request->contact;
        $user_details[0]->gender           = $request->gender;
        $user_details[0]->marital_status   = $request->e_civil;
        $user_details[0]->about_me         = $request->about;
        $user_details[0]->birthday         = $request->birthday;
        $user_details[0]->save();
        return redirect()->route('usuarios.show', compact('user','user_details'))->with('success', "Detalhes do usuario actualizados");
    }
}
