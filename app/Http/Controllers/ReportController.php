<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use DB;

class ReportController extends Controller{

    public function showReports(){
        return view('vendor.reports.report', compact(''));
    }
}
