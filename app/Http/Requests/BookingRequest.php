<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'departure'         => 'required|max:255',
            'arrival'           => 'required|max:255',
            'book_type'         => 'required',
            'departure_date'    => 'required',
            'passenger_numb'    => 'required',
            'return_date'       => 'required_if:book_type,Both',
        ];
    }

    public function messages(){
        return [
            'departure.required'         => 'Selecione a provincia de partida',
            'arrival.required'           => 'Selecione o destino',
            'book_type.required'         => 'Selecione o tipo de viagem',
            'departure_date.required'    => 'Escolha o dia de partida',
            'passenger_numb.required'    => 'Numero de passageiros',
            'return_date.required_if'    => 'Escolha a data de regresso',
        ];
    }
}
