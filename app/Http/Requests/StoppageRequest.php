<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoppageRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'stoppage_route'    => 'required|max:255',
            'stoppage_province' => 'required|max:255',
            'stoppage_name'     => 'required'
        ];
    }

    public function messages(){
        return [
            'stoppage_route.required'    => 'Escolha a rota',
            'stoppage_province.required' => 'Escolha a provincia de paragem',
            'stoppage_name.required'     => 'Escreva o nome da paragem',
        ];
    }
}
