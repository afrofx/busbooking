<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'reference'     => 'required|unique:buses|max:255',
            'car_brand'         => 'required|max:255',
            'car_model'         => 'required',
            'data-registo'      => 'required',
            'license_plate'     => 'required|unique:buses|max:255',
            'car_fuel_type'     => 'required',
            'car_transmission'  => 'required',
            'car_photo'         => 'required',
            'car_state'         => 'required',
        ];
    }

    public function messages(){
        return [
            'reference.required'         => 'Escreva a referência do automovel',
            'car_brand.required'         => 'Escreva a marca do automovel',
            'car_model.required'         => 'Escreva o modelo do automovel',
            'data-registo.required'      => 'Escreva a data de registo',
            'license_plate.required'     => 'Escreva a matricula do automovel',
            'car_fuel_type.required'     => 'Escolha o tipo de combustivel',
            'car_transmission.required'  => 'Escolha o tipo de transmissão',
            'car_photo.required'         => 'Escolha a foto do automovel',
            'car_state.required'         => 'Selecione o estado do automovel',
            'license_plate.unique'       => 'Já Existe um automovel com essa matricula',
            'reference.unique'           => 'A referencia ja foi registada'
        ];
    }
}
