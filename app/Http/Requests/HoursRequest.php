<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HoursRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'departure_route'   => 'required|max:255',
            'departure_day'     => 'required|max:255',
            'departure_time'    => 'required',
        ];
    }

    public function messages(){
        return [
            'departure_route.required'   => 'Escolha a rota',
            'departure_day.required'     => 'Escolha o dia de partida',
            'departure_time.required'    => 'Escolha a hora de partida',
        ];
    }
}
