<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'op_photo'      => 'required|max:1024',
            'name'          => 'required|max:255',
            'username'      => 'required|unique:users|max:20',
            'id_number'     => 'required|unique:user_details|max:20',
            'gender'        => 'required',
            'birthday'      => 'required',
            'address'       => 'required',
            'm_status'      => 'required',
            'email'         => 'required|unique:users',
            'password'      => 'required',
            'role'          => 'required'
        ];
    }

    public function messages(){
        return [
            'op_photo.required'     => 'Escolha a imagem do perfil',
            'name.required'         => 'Escreva o primeiro nome',
            'username.required'      => 'Escreva o apelido',
            'id_number.required'    => 'Escreva o numero do Bilhete de Identidade',
            'id_number.unique'      => 'O numero do bilhete de identidade ja se encontra registado',
            'gender.required'       => 'Escreva a idade',
            'birthday.required'     => 'Escreva a data de registo',
            'address.required'      => 'Escreva o endereço',
            'm_status.required'     => 'Selecione o estado do usuário',
            'email.required'        => 'Escreva seu email',
            'email.unique'          => 'O email ja foi registado',
            'password.required'     => 'Escreva sua palavra passe',
            'role.required'         => 'Selecione o cargo do usuario'
        ];
    }
}
