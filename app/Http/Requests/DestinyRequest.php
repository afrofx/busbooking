<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DestinyRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'departure'         => 'required|max:255',
            'arrival'           => 'required|max:255',
            'ticket_price'      => 'required|max:255',
            'route_state'       => 'required|max:255',
            'car'               => 'required|max:255'
        ];
    }

    public function messages(){
        return [
            'departure.required'         => 'Selecione a origem',
            'arrival.required'           => 'Selecione o destino',
            'ticket_price.required'      => 'Escreva o preço do bilhete',
            'route_state.required'       => 'Selecione o estado da rota',
            'car.required'               => 'Selecione o automovel  que ira circular na rota'
        ];
    }
}
