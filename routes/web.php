<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/acerca', 'HomeController@about');
Route::get('/destinos', 'HomeController@DestinyList');
Route::get('/destinyList', 'HomeController@findDestinyList');
Route::get('/minhareserva', 'HomeController@showBook');
Route::get('/passagens/reservar', 'HomeController@saveBook');
Route::get('/minhareserva/bilhete', 'HomeController@ticket');

Route::post('/passagens', 'HomeController@searchBook');
Route::post('/saveBooking', 'HomeController@storePassenger');
Route::POST('/minhareserva/mostrar', 'HomeController@seeReservation');

Route::post('/addStoppage', array('uses' => 'DestinyController@addStoppage'));
Route::post('/addTimes', array('uses' => 'DestinyController@addTimes'));

// --------Admin Routes-----------------
Route::group(['middleware' => ['auth']], function () {
    Route::get('/admin', 'AdminController@index')->name('admin.index');
});

// --------Cars Routes-----------------
Route::group(['middleware' => ['auth']], function () {
    Route::get('admin/autocarros/', 'CarsController@index')->name('autocarros.index');
    Route::get('admin/autocarros/adicionar', 'CarsController@create')->name('autocarros.create');
    Route::get('admin/autocarros/{id}', 'CarsController@show')->name('autocarros.show');
    Route::get('admin/autocarros/{id}/editar', 'CarsController@edit')->name('autocarros.edit');
    Route::post('admin/autocarros/guardar', 'CarsController@store')->name('autocarros.store');
    Route::post('admin/autocarros/{id}/actualizar', 'CarsController@update')->name('autocarros.update');
    Route::delete('admin/autocarros/{id}/remover', 'CarsController@destroy')->name('autocarros.destroy');
});

// --------Users Routes-----------------
Route::group(['middleware' => ['auth']], function () {
    Route::get('admin/usuarios/', 'DriversController@index')->name('usuarios.index');
    Route::get('admin/usuarios/adicionar', 'DriversController@create')->name('usuarios.create');
    Route::get('admin/usuarios/{id}', 'DriversController@show')->name('usuarios.show');
    Route::get('admin/usuarios/{id}/editar', 'DriversController@edit')->name('usuarios.edit');
    Route::post('admin/usuarios/guardar', 'DriversController@store')->name('usuarios.store');
    Route::post('admin/usuarios/{id}/actualizar', 'DriversController@update')->name('usuarios.update');
    Route::post('admin/usuarios/{id}/actualizar/detalhes', 'DriversController@updateDetails')->name('usuarios.update.details');
    Route::post('admin/usuarios/{id}/actualizar/dados', 'DriversController@updateUserData')->name('usuarios.update.data');
    Route::delete('admin/usuarios/{id}/remover', 'DriversController@destroy')->name('usuarios.destroy');
    Route::get('/admin/perfil', 'DriversController@UserProfile')->name('usuarios.profile');
    Route::post('/admin/perfil/actualizar/{id}/data', 'DriversController@updateUser')->name('usuarios.profile.data');
    Route::post('/admin/perfil/actualizar/{id}', 'DriversController@updateUserInfo')->name('usuarios.profile.details');
});

// --------Routes Routes-----------------
Route::group(['middleware' => ['auth']], function () {
    Route::get('admin/rotas/', 'DestinyController@routes')->name('rotas.index');
    Route::get('admin/rotas/paragens', 'DestinyController@stoppages')->name('rotas.paragens');
    Route::get('admin/rotas/horarios', 'DestinyController@hours')->name('rotas.horarios');
    Route::post('admin/rotas/paragens/guardar', 'DestinyController@stoppagesSave')->name('rotas.paragens.save');
    Route::post('admin/rotas/horarios/guardar', 'DestinyController@hoursSave')->name('rotas.horarios.save');
    Route::post('admin/rotas/guardar', 'DestinyController@routesSave')->name('rotas.save');
    Route::delete('admin/rotas/paragens/{id}/eliminar', 'DestinyController@stoppagesRemove')->name('rotas.paragens.destroy');
    Route::delete('admin/rotas/horarios/{id}/eliminar', 'DestinyController@hoursRemove')->name('rotas.horarios.destroy');
    Route::delete('admin/rotas/{id}/eliminar', 'DestinyController@routesRemove')->name('rotas.destroy');
});

// --------Routes Routes-----------------
Route::group(['middleware' => ['auth']], function () {
    Route::get('admin/reservas/', 'BookingController@index')->name('reservas.index');
    Route::get('admin/reservas/adicionar', 'BookingController@create')->name('reservas.create');
    Route::post('admin/reservas/guardar', 'BookingController@store')->name('reservas.store');
    Route::post('admin/reservas/cancelar', 'BookingController@cancelBooking')->name('reservas.cancel');
    Route::delete('admin/reservas/{id}/eliminar', 'BookingController@destroy')->name('reservas.destroy');
});



