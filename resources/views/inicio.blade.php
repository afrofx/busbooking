@extends('layouts.root')

@section('title', 'Bus Booking')


@section('content')


<div class="home-section container">
        <a class="weatherwidget-io" href="https://forecast7.com/pt/n25d9732d57/maputo/" data-label_1="MAPUTO" data-label_2="TEMPERATURA" data-theme="pure" >MAPUTO TEMPERATURA</a>
        <script>
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
        </script>
    <div class="mt-3">
        
@if ($errors->any())
<div class="alert alert-danger">
    <h4>Erro ao submeter</h4><hr>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
        <div class="row">
            <div class="col-md-6">
                <div class="card rounded-0">
                    <form action="{{ action('HomeController@searchBook')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <h3 class="card-title m-0 mb-3"><i class="lni-pencil-alt"></i>Efectuar Reserva</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="province">Origem:</label>
                                        <select class="form-control" oninvalid="this.setCustomValidity('Escolha a Origem')"
                                        oninput="this.setCustomValidity('')" id="province" name="departure" required>
                                            <option value="" disabled selected hidden>Selecione a Origem</option>
                                            @foreach ($provinces as $province)
                                                <option value="{{$province->province_id}}">{{$province->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="province">Destino:</label>
                                        <select oninvalid="this.setCustomValidity('Escolha o Destino')"
                                        oninput="this.setCustomValidity('')" class="form-control" id="destiny" name="arrival" required>
                                            <option value="" disabled selected hidden>Selecione o Destino</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input onchange="findselected()" type="radio" id="customRadioInline1" name="book_type" class="custom-control-input" value="Single">
                                            <label class="custom-control-label" for="customRadioInline1">Apenas Ida</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input onchange="findselected()" type="radio" id="customRadioInline2" name="book_type" class="custom-control-input" value="Both">
                                            <label class="custom-control-label" for="customRadioInline2">Ida e Volta</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="date" oninvalid="this.setCustomValidity('Data de Partida')"
                                                oninput="this.setCustomValidity('')" name="departure_date" class="form-control" placeholder="Data de Partida" required>
                                                <script>
                                                    var today = new Date().toISOString().split('T')[0];
                                                    document.getElementsByName("departure_date")[0].setAttribute('min', today);
                                                </script>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="date" name="return_date"  class="form-control" placeholder="Data de Volta" id="return">
                                                <script>
                                                    var today = new Date().toISOString().split('T')[0];
                                                    document.getElementsByName("return_date")[0].setAttribute('min', today);
                                                </script>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <input class="form-control" oninvalid="this.setCustomValidity('Indique o número de passageiros')"
                                        oninput="this.setCustomValidity('')" name="passenger_numb" type="number" placeholder="Passageiros" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input class="btn btn-secondary mt-3" type="reset" value="Limpar Campos">
                        <input class="btn btn-primary mt-3" type="submit" value="Efectuar Reserva">
                    </form>
                </div>
            </div>

            <div class="col-md-6">
                <table class="table gradient-table">
                    <thead class="gradient-table">
                        <tr>
                            <th class="py-0" colspan="2">Origem</th>
                            <th class="py-0">Destino</th>
                            <th class="py-0">Preço</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($routes as $route)
                        <tr>
                                @foreach ($provinces as $province)
                                    @if ($province->province_id == $route->from)
                                        <td class="py-0">{{$province->name}}</td>
                                        <td class="py-0"><b><i class="lni-arrow-right"></i></b></td>
                                    @endif
                                    @if ($province->province_id == $route->to)
                                        <td class="py-0">{{$province->name}}</td>
                                    @endif
                                @endforeach
                                
                            <td class="py-0">{{$route->price}} MZN</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="province mt-3">
        <div class="row">
            <div class="col-md-6">
                <p class="text-white text-xl">10 Provincias, <span class="text-warning">{{$routes->count()}} Rotas</span></p>
            </div>
            <div class="col-md-6">
                <button class="btn btn-warning"><a href="http://" target="" class="link-null">Reserve Já</a></button>
            </div>
        </div>
    </div>

    <div class="mt-3">
        <div class="row">
            <div class="col-md-6">
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-success">Zona Sul</strong>
                        <h3 class="mb-0">Maputo</h3>
                        <p class="mb-auto"> A província de Maputo é a mais meridional das províncias de Moçambique. A sua capital é a cidade de Matola, situada a apenas de 10  km a oeste da cidade de Maputo, a capital do país.</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/Maputo.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-success">Zona Sul</strong>
                        <h3 class="mb-0">Gaza</h3>
                        <p class="mb-auto">Gaza é uma província de Moçambique. Localizada no sul do país e a sua capital é a cidade de Xai-Xai, situada a cerca de 210  quilómetros a norte da capital nacional, Maputo.</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/gaza.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-success">Zona Sul</strong>
                        <h3 class="mb-0">Inhambane</h3>
                        <p class="mb-auto">A província de Inhambane, está localizada região sul de Moçambique. A sua capital é a cidade de Inhambane, situada a cerca de 500 km a norte da cidade de Maputo.</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/inhambane.jpg">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-danger">Zona Centro</strong>
                        <h3 class="mb-0">Manica</h3>
                        <p class="mb-auto">A província de Manica está localizada na região centro de Moçambique. A sua capital é a cidade de Chimoio, a cerca de 1100 km a norte de Maputo.</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/manica.jpg">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-danger">Zona Centro</strong>
                        <h3 class="mb-0">Sofala</h3>
                        <p class="mb-auto">Sofala é uma província de Moçambique. Situa-se na região centro do país, com uma longa costa, numa reentrância do canal de Moçambique.</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/sofala.jpg">
                    </div>
                </div>
            </div>

            <div class="col-md-6"> 
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-danger">Zona Centro</strong>
                        <h3 class="mb-0">Tete</h3>
                        <p class="mb-auto">Tete é uma província da região central de Moçambique, A sua capital é a cidade de Tete, localizada a cerca de 1570  km a norte da cidade de Maputo, a capital do país.</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/tete.jpg">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-danger">Zona Centro</strong>
                        <h3 class="mb-0">Zambezia</h3>
                        <p class="mb-auto">Zambézia é uma província situada na região centro de Moçambique. A sua capital é a cidade de Quelimane, localizada a cerca de 1 600 quilómetros ao norte de Maputo, a capital do país.</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/zambezia.jpg">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-warning">Zona Norte</strong>
                        <h3 class="mb-0">Nampula</h3>
                        <p class="mb-auto">Nampula é uma província situada na região norte de Moçambique. A sua capital é a cidade de Nampula, localizada a cerca de 2150  km a norte da cidade de Maputo, a capital do país.</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/nampula.jpg">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-warning">Zona Norte</strong>
                        <h3 class="mb-0">Niassa</h3>
                        <p class="mb-auto">A província do Niassa é uma subdivisão de Moçambique, situada no extremo noroeste do país. Tem capital na cidade de Lichinga. É a maior província de Moçambique em termos de área — 129 056 km²</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/niassa.jpg">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row no-gutters overflow-hidden flex-md-row mb-4 h-md-250 position-relative  card p-0">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-warning">Zona Norte</strong>
                        <h3 class="mb-0">Cabo Delgado</h3>
                        <p class="mb-auto">A província de Cabo Delgado é uma subdivisão de Moçambique localizada no extremo nordeste do país. A sua capital é a cidade de Pemba, localizada a cerca de 2 600 km norte de Maputo, a capital do país.</p>
                        <a href="#" class="stretched-link">Continuar a ler</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img class="bd-placeholder-img" width="200" height="250" src="images/provinces/cabo.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop