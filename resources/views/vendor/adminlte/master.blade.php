<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
@yield('title', config('adminlte.title', 'Bus Booking'))
@yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
   {{-- --------------------------Fontawesome Icons----------------------------- --}}
   <link rel="stylesheet" href="{{asset('vendor/adminlte/vendor/assets/fonts/fontawesome/css/fontawesome.css')}}" rel="stylesheet">
   <link rel="stylesheet" href="{{asset('vendor/adminlte/vendor/assets/fonts/fontawesome/css/brands.css')}}" rel="stylesheet">
   <link rel="stylesheet" href="{{asset('vendor/adminlte/vendor/assets/fonts/fontawesome/css/solid.css')}}" rel="stylesheet">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">
    
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/assets/icons/lineicons/LineIcons.min.css') }}">

    {{-- <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/assets/plugins/datatables/default/css/dataTables.bootstrap4.min.css') }}"> --}}

    @include('adminlte::plugins', ['type' => 'css'])

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">

    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet">    
    <link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet">


    @yield('adminlte_css')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<style>
    
    .card{
        padding: 1.2rem;
        border-radius: 4px;
        background-color: #fff;
        box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0,0,0,.12);     
    }

</style>
<body class="hold-transition @yield('body_class')">

@yield('body')
<script src="{{ asset('vendor/adminlte/vendor/assets/fonts/fontawesome/js/brands.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/assets/fonts/fontawesome/js/solid.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/assets/fonts/fontawesome/js/fontawesome.js') }}"></script>


<script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('vendor/adminlte/vendor/assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

<script src="{{ asset('vendor/adminlte/vendor/assets/plugins/jspdf/jspdf.debug.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/assets/plugins/jspdf/jspdf.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/assets/plugins/jspdf/autotable.min.js') }}"></script>

<script src="{{ asset('vendor/adminlte/vendor/assets/plugins/datatables/default/js/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('vendor/adminlte/vendor/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.pt.min.js') }}"></script>

<script src="{{ asset('js/js.js') }}"></script>


<script src="{{ asset('vendor/adminlte/vendor/assets/js/admin-main.js') }}"></script>

@include('adminlte::plugins', ['type' => 'js'])

@yield('adminlte_js')

</body>
</html>
