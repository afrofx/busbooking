@extends('adminlte::page')

@section('title', 'Gerar Relatórios')

@section('content_header')
    <h1>Gerar Relatórios</h1>
@stop

@section('content')


<div class="callout callout-info">
	<h4>Informação!</h4>
	<p>Crie, todo tipo de relatórios, preenchendo os campos abaixo</p>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
		<h4>Erros encontrados</h4><hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card">
    <form>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="report_type">Tipo de relatório</label>
                <select id="report_type" name="report_type" class="form-control">
                    <option selected disabled>Escolher relatório</option>
                    <option value="R001">Reservas Efectuadas</option>
                    <option value="R002">Reservas Pendentes</option>
                    <option value="R003">Reservas Pagas</option>
                    <option value="R004">Reservas Canceladas</option>
                    <option value="R005">Rotas e Autocaros</option>
                    <option value="R006">Todas as Rotas</option>
                    <option value="R007">Autocarros Registados</option>
                    <option value="R008">Autocarros Operacionais</option>
                    <option value="R009">Motoristas</option>
                    <option value="R010">Usuarios do Sistema</option>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="date_start">Data Inicio</label>
                <input type="date" class="form-control" name="date_start" id="date_start" placeholder="Data de Inicio">
            </div>

            <div class="form-group col-md-4">
                <label for="date_end">Data Fim</label>
                <input type="date" class="form-control" name="date_end" id="date_end" placeholder="Data Fim">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Gerar Relatório</button>
    </form>
</div>



@stop