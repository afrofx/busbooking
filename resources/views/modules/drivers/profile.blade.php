@extends('adminlte::page')

@section('title', 'Perfil: '.$user->name)

@section('content_header')
    <h1>Detalhes do usuário</h1>
@stop


<style>

.profile-user-img{
	height: 100px !important;
	width: 100px !important;
}

</style>

@section('content')

    <div class="row">
		<div class="col-md-12">
			@if ($message = Session::get('success'))
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-check"></i> Alerta </h4>
					{{$message}}
				</div>
			@endif
		</div>
		<div class="col-md-3">
			<div class="box box-primary">
				<div class="box-body box-profile">
					<img class="profile-user-img img-responsive img-circle" src="../storage/images/users/{{$user_details->profile_photo}}" alt="User profile picture">

					<h3 class="profile-username text-center">{{$user->name}}</h3>

					<p class="text-muted text-center">{{$user->roles[0]->name}}</p>

					<ul class="list-group list-group-unbordered">
						<li class="list-group-item">
							<b>Ultimo Login</b> <a class="pull-right">{{$user->last_login_at}}</a>
						</li>
						<li class="list-group-item">
							<b>Endereço IP</b> <a class="pull-right">{{$user->last_login_ip}}</a>
						</li>
					</ul>
				  </div>
			</div>
		
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Sobre mim</h3>
				</div>
				
				<div class="box-body">
					<strong><i class="fa fa-book margin-r-5"></i> Biográfia</strong>
					
					<p class="text-muted">{{$user_details->about_me}}</p>

					<hr>

					<strong><i class="fa fa-map-marker margin-r-5"></i> Endereço</strong>

					<p class="text-muted">{{$user_details->address}}</p>

					<hr>

					<strong><i class="fa fa-phone margin-r-5"></i> Contactos</strong>

					<ul class="list-group list-group-unbordered">
						<li class="list-group-item">
							<b>Email</b> <a class="pull-right">{{$user->email}}</a>
						</li>
						<li class="list-group-item">
							<b>Contacto</b> <a class="pull-right">{{$user_details->contact}}</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		
		<div class="col-md-9">
			<div class="nav-tabs-custom">

				<ul class="nav nav-tabs">
					<li class="active"><a href="#settings" data-toggle="tab" aria-expanded="true">Detalhes de Autentição</a></li>
					<li class="inactive"><a href="#info" data-toggle="tab" aria-expanded="false">Detalhes da Adicionais</a></li>
				</ul>
			
				<div class="tab-content">
					<div class="tab-pane active" id="settings">
						<form class="form-horizontal" action="{{ route('usuarios.profile.data', $user->id)}}" method="POST" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="name" class="col-sm-2 control-label">Nome Completo</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="name" id="name" placeholder="Nome Completo" value="{{$user->name}}">
								</div>
							</div>

							<div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{$user->email}}">
								</div>
							</div>

							<div class="form-group">
								<label for="username" class="col-sm-2 control-label">Username</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="username" id="username" placeholder="Nome do usuario" value="{{$user->username}}">
								</div>
							</div>

							<div class="form-group">
								<label for="password" class="col-sm-2 control-label">Nova Senha</label>
								<div class="col-sm-10">
									<input type="password" class="form-control" name="password" id="password" placeholder="Digite a nova senha">
								</div>
							</div>

							<div class="form-group">
								<label for="confirm_password" class="col-sm-2 control-label">Confirmar Senha</label>
								<div class="col-sm-10">
									<input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="Repetir a senha">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-success">Actualizar</button>
								</div>
							</div>

						</form>
					</div>
			
			
					<div class="tab-pane" id="info">
						<form class="form-horizontal" action="{{ route('usuarios.profile.details', $user_details->id)}}" method="POST" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="image" class="col-sm-3 control-label">Foto de Perfil</label>
								<div class="col-sm-9">
									<input type="file" class="form-control" name="image" id="image" placeholder="Imagem de perfil">
								</div>
							</div>

							<div class="form-group">
								<label for="identity" class="col-sm-3 control-label">Bilhete de Identidade</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="identity" id="identity" placeholder="Bilhete de Identidade" value="{{$user_details->id_number}}">
								</div>
							</div>

							<div class="form-group">
								<label for="birthday" class="col-sm-3 control-label">Data de Nascimento</label>
								<div class="col-sm-9">
									<input type="date" class="form-control" name="birthday" id="birthday" placeholder="Data de nascimento" value="{{$user_details->birthday}}">
								</div>
							</div>
						
							<div class="form-group">
								<label for="gender" class="col-sm-3 control-label">Sexo</label>
								<div class="col-sm-9">
									<select class="form-control" name="gender" id="gender">
										<option @if ($user_details->gender === null) selected @endif  disabled>Selecionar sexo</option>
										<option @if ($user_details->gender === "Femenino") selected @endif value="Femenino">Femenino</option>
										<option @if ($user_details->gender === "Masculino") selected @endif value="Masculino">Masculino</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="e_civil" class="col-sm-3 control-label">Estado Civil</label>
								<div class="col-sm-9">
									<select class="form-control" name="e_civil" id="e_civil">
										<option @if ($user_details->marital_status === null) selected @endif disabled>Selecionar Estado civil</option>
										<option @if ($user_details->marital_status === "Solteiro") selected @endif value="Solteiro">Solteiro</option>
										<option @if ($user_details->marital_status === "Casado") selected @endif value="Casado">Casado</option>
									</select>
								</div>
							</div>
						
							<div class="form-group">
								<label for="contact" class="col-sm-3 control-label">Contacto</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="contact" id="contact" placeholder="Contacto" value="{{$user_details->contact}}">
								</div>
							</div>
							
							<div class="form-group">
								<label for="address" class="col-sm-3 control-label">Endereço</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="address" name="address" placeholder="Morada" value="{{$user_details->address}}">
								</div>
							</div>

							<div class="form-group">
								<label for="about" class="col-sm-3 control-label">Acerca de mim</label>
								<div class="col-sm-9">
									<textarea class="form-control" id="about" name="about" placeholder="Acerca de mim">{{$user_details->about_me}}</textarea>
								</div>
							</div>
						
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-success">Actualizar</button>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
    </div>

@stop