@extends('adminlte::page')

@section('title', 'Lista de Usuarios')

@section('content_header')
    <h1>Usuarios Registados</h1>
@stop

<link rel="stylesheet" href="../css/table-style.css">

@section('content')


<script type="text/javascript">
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="alert alert-info alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-info"></i> Informação!</h4>
	Encontre a lista de usuarios registados na transportadora
</div>

@if ($message = Session::get('success'))
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Alerta </h4>
		{{$message}}
	</div>
@endif

<div class="table-wrapper">
	<div class="table-title">
		<div class="row">
			<div class="col-sm-4">
				<h2><b>Usuários</b> Registados no Sistema</h2>
			</div>
			<div class="col-sm-8">						
				<a href="#" class="btn btn-primary"><i class="material-icons">&#xE863;</i> <span>Actualizar Lista</span></a>
				<a onclick="printUsers()" class="btn btn-info"><i class="material-icons">&#xE24D;</i> <span>Imprimir Lista</span></a>
			</div>
		</div>
	</div>
	<table id="dataTable" class="table table-striped table-hover display">
		<thead>
			<tr>
				<th>Foto</th>
				<th>Nome</th>						
				<th>Tipo</th>				
				<th>Email</th>	
				<th>Data de Registo</th>
				<th>Operação</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $user)
			<tr>
				<td><a href="#" class="pop"><img id="myImg" src="../storage/images/users/@foreach ($user_details as $user_detail)
					@if ($user_detail->user_id === $user->id)
						{{$user_detail->profile_photo}}
					@endif
				@endforeach" class="avatar" alt="Avatar"></a> </td>
				<td>{{$user->name}}</td>
				<td>{{$user->roles[0]->name}}</td>                  
				<td>{{$user->email}}</td>
				<td>{{$user->created_at}}</td>
				<td>
					<a href="{{ url('admin/usuarios/'.$user->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
					<a href="{{ url('admin/usuarios/'.$user->id.'/editar') }}" class="btn btn-sm btn-warning text-white"><i class="fa fa-pen"></i></a>
					{!! Form::open(['method' => 'DELETE','route' => ['usuarios.destroy', $user->id],'style'=>'display:inline']) !!}
						<button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
					{!! Form::close() !!}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
		




<table id="table-print tablesUsers" class="table-print" style="display: none">
	<thead>
		<tr style="background: rgb(231, 76, 60); color: white;">
			<th>#</th>
			<th>Nome</th>
			<th>Cargo</th>				
			<th>Email</th>
			<th>Data de Registo</th>	
		</tr>
	</thead>
	<tbody>
		@forelse ($users as $user)
			<tr>
				<td>{{$user->name}}</td>
				<td>{{$user->roles[0]->name}}</td>                  
				<td>{{$user->email}}</td>
				<td>{{$user->created_at}}</td>
			</tr>
		@empty
			<tr> <td colspan="6"> Sem Registos </td></tr>
		@endforelse
	</tbody>
</table>
@stop