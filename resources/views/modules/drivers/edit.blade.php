@extends('adminlte::page')

@section('title', 'Editar Usuario')

@section('content_header')
    <h1>Editar: {{$user->name}}</h1>
@stop
@section('content')
<div class="callout callout-info">
	<h4>Informação!</h4>
	<p>Actualize dados do usuario modificando os campos abaixo</p>
</div>

<style>
	h3{
		margin-top: 0%;
	}
	
</style>

@if ($errors->any())
    <div class="alert alert-danger">
		<h4>Erro ao submeter</h4><hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form  action="{{route('usuarios.update', $user->id)}}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<h3 class="card-title m-0 "><i class="lni-pencil-alt"></i>Dados do Usuário</h3>
			<div class="row">
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>Imagem de Perfil</b></span>
						<input type="file" name="op_photo"  accept="image/jpeg, image/png, image/gif" class="form-control" placeholder="Imagens do Autocarro">
					</div>
					
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>P.N.</b></span>
						<input name="name" type="text" class="form-control" placeholder="Nome Completo" value="{{$user->name}}">
					</div>
					
					<br>
					
					<div class="input-group">
						<span class="input-group-addon"><b>N.U</b></span>
						<input name="username" type="text" class="form-control" placeholder="Ex: manuel" value="{{$user->username}}">
					</div>
					
					<br>
					
					<div class="input-group">
						<span class="input-group-addon"><b>B.I.</b></span>
						<input name="id_number" type="text" class="form-control" placeholder="Número do B.I" value="{{$user_details[0]->id_number}}">
					</div>
					
				</div>
				
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>S.X.</b></span>
						<select class="form-control" name="gender">
							<option disabled selected hidden value="">Género</option>
							<option @if ($user_details[0]->gender == "Masculino") selected @endif value="Masculino">Masculino</option>
							<option @if ($user_details[0]->gender == "Femenino") selected @endif value="Femenino">Femenino</option>
						</select>
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>D.N.</b></span>
						<input type="text" name="birthday" class="form-control datepicker" placeholder="Data de Nascimento" value="{{$user_details[0]->birthday}}">
					</div>

					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>L.R.</b></span>
						<input type="text" class="form-control" name="address" placeholder="Local de Residência" value="{{$user_details[0]->address}}" >
					</div>

					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>E.C.</b></span>
						<select class="form-control" name="m_status">
							<option disabled selected hidden value="">Estado Civil</option>
							<option @if ($user_details[0]->marital_status == "Solteiro") selected @endif value="Solteiro">Solteiro</option>
							<option @if ($user_details[0]->marital_status == "Casado") selected @endif value="Casado">Casado</option>
						</select>
					</div>
					
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>C.N.</b></span>
						<input type="text" class="form-control" name="contact" placeholder="Ex: 841212123" value="{{$user_details[0]->contact}}">
					</div>
				</div>

				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>C.E.</b></span>
						<input type="text" name="email" class="form-control" placeholder="Correio Electronico" value="{{$user->email}}">
					</div>

					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>S.A.</b></span>
						<input type="text" name="password" class="form-control" placeholder="Senha de Acesso">
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-user"></i></span>
						<select class="form-control" name="role">
							<option disabled selected hidden value="">Selecionar Cargo</option>
							@foreach ($roles as $role)
								<option @if ($user->roles[0]->id == $role->id) selected @endif value="{{$role->id}}">{{$role->name}}</option>
							@endforeach
						</select>
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>S.M.</b></span>
						<input type="text" class="form-control" name="about" placeholder="Detalhes adicionais" value="{{$user_details[0]->about_me}}">
					</div>

					<br>
					<div class="input-group">
						<button type="reset" class="btn btn-warning">Limpar Campos</button>
						<button type="submit" class="btn btn-success float-right">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
@stop