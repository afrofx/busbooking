@extends('adminlte::page')

@section('title', 'Adicionar Usuario')

@section('content_header')
    <h1>Adicionar novos usuarios</h1>
@stop
@section('content')
<div class="callout callout-info">
	<h4>Informação!</h4>
	<p>Adicione novos usuarios a base de dados</p>
</div>

<style>
	h3{
		margin-top: 0%;
	}
	
</style>

@if ($errors->any())
    <div class="alert alert-danger">
		<h4>Erro ao submeter</h4><hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form  action="{{route('usuarios.store')}}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<h3 class="card-title m-0 "><i class="lni-pencil-alt"></i>Dados do Usuário</h3>
			<div class="row">
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>Imagem de Perfil</b></span>
						<input type="file" name="op_photo"  accept="image/jpeg, image/png, image/gif" class="form-control" placeholder="Imagens do Autocarro">
					</div>
					
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>P.N.</b></span>
						<input name="name" type="text" class="form-control" placeholder="Nome Completo" required>
					</div>
					
					<br>
					
					<div class="input-group">
						<span class="input-group-addon"><b>N.U</b></span>
						<input name="username" type="text" class="form-control" placeholder="Ex: manuel" required>
					</div>
					
					<br>
					
					<div class="input-group">
						<span class="input-group-addon"><b>B.I.</b></span>
						<input name="id_number" type="text" class="form-control" placeholder="Número do B.I" required>
					</div>
					
				</div>
				
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>S.X.</b></span>
						<select class="form-control" name="gender" required>
							<option disabled selected>Género</option>
							<option value="Masculino">Masculino</option>
							<option value="Femenino">Femenino</option>
						</select>
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>D.N.</b></span>
						<input type="text" name="birthday" class="form-control datepicker" placeholder="Data de Nascimento" required>
					</div>

					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>L.R.</b></span>
						<input type="text" class="form-control" name="address" placeholder="Local de Residência" required>
					</div>

					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>E.C.</b></span>
						<select class="form-control" name="m_status" required>
							<option disabled selected>Estado Civil</option>
							<option value="Solteiro">Solteiro</option>
							<option value="Casado">Casado</option>
						</select>
					</div>
					
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>C.N.</b></span>
						<input type="text" class="form-control" name="contact" placeholder="Ex: 841212123" required>
					</div>
				</div>

				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>C.E.</b></span>
						<input type="text" name="email" class="form-control" placeholder="Correio Electronico" required>
					</div>

					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>S.A.</b></span>
						<input type="text" name="password" class="form-control" placeholder="Senha de Acesso" required>
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-user"></i></span>
						<select class="form-control" name="role">
							<option disabled selected>Selecionar Cargo</option>
							@foreach ($roles as $role)
								<option value="{{$role->id}}">{{$role->name}}</option>
							@endforeach
						</select>
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>S.M.</b></span>
						<input type="text" class="form-control" name="about" placeholder="Detalhes adicionais">
					</div>

					<br>
					<div class="input-group">
						<button type="reset" class="btn btn-warning">Limpar Campos</button>
						<button type="submit" class="btn btn-success float-right">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
@stop