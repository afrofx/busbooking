@extends('adminlte::page')

@section('title', 'Lista de rotas')

@section('content_header')
    <h1>Rotas registadas</h1>
@stop

@section('content')
<div class="callout callout-info">
	<h4>Informação!</h4>
	<p>Encontre a lista de todas as rotas disponiveis na transportadora</p>
</div>

<div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<div class="box-header">
		  <h3 class="box-title">Lista de rotas</h3>

		  <div class="box-tools">
			<div class="input-group input-group-sm hidden-xs" style="width: 150px;">
			  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

			  <div class="input-group-btn">
				<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
			  </div>
			</div>
		  </div>
		  
		  <button class="btn btn-sm btn-primary" onclick="printRoutes()">Imprimir Lista</button>
		</div>
		<!-- /.box-header -->
		<div class="box-body table-responsive no-padding">
		  <table class="table table-hover">
			<tbody><tr>
				<th>ID</th>
				<th>Origem</th>
				<th>Destino</th>
				<th>Preço</th>
				<th>Estado</th>
				<th>Automovel</th>
				<th>Acções</th>
			</tr>
			@forelse ($routes as $route )
			<tr>
				<td>{{$route->route_id}}</td>
				@foreach ($provinces as $province)
					@if ($province->province_id == $route->from)
						<td>{{$province->name}}</td>
					@endif
				@endforeach
				@foreach ($provinces as $province)
					@if ($province->province_id == $route->to)
						<td>{{$province->name}}</td>
					@endif
				@endforeach
				<td>{{$route->price}}</td>
				<td>
					@if ($route->state == 'Active')
						<span class="label label-success">Disponivel</span>	
					@else
						<span class="label label-danger">Indisponivel</span>
					@endif
					
				</td>
				<td>{{$route->license_plate}}</td>
			</tr>
			@empty
			<tr>
				Sem Rotas Registadas
			</tr>
			@endforelse
			
		  </tbody></table>
		</div>
		<!-- /.box-body -->
	  </div>
	  <!-- /.box -->
	</div>
  </div>



  <table id="table-print" class="table-print" style="display: none">
		<thead>
			<tr style="background: rgb(231, 76, 60); color: white;">
				<th>##</th>
				<th>Origem</th>
				<th>Destino</th>
				<th>Preço</th>
				<th>Estado</th>
				<th>Automovel</th>
			</tr>
		</thead>
		<tbody>
				@forelse ($routes as $route )
			<tr>
				<td>{{$route->route_id}}</td>
				@foreach ($provinces as $province)
					@if ($province->province_id == $route->from)
						<td>{{$province->name}}</td>
					@endif
				@endforeach
				@foreach ($provinces as $province)
					@if ($province->province_id == $route->to)
						<td>{{$province->name}}</td>
					@endif
				@endforeach
				<td>{{$route->price}}</td>
				<td>
					@if ($route->state == 'Active')
						<span class="label label-success">Disponivel</span>	
					@else
						<span class="label label-danger">Indisponivel</span>
					@endif
					
				</td>
				<td>{{$route->license_plate}}</td>
			</tr>
			@empty
			<tr>
				Sem Rotas Registadas
			</tr>
			@endforelse
		</tbody>
	</table>
	
@stop