@extends('adminlte::page')

@section('title', 'Adicionar Rota')

@section('content_header')
    <h1>Adicionar Rota</h1>
@stop

@section('content')
<div class="callout callout-info">
	<h4>Informação!</h4>
	<p>Adicione novas rotas e aloque autocarros as mesmas</p>
</div>


<style>
	h3{
		margin-top: 0%;
	}
	
</style>
<form action="../../admin/destinos" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
<div class="row">
	<div class="col-md-12" style="margin-bottom: 1.2rem">
		<div class="card">
			<h3 class="card-title" style="margin: 0"><i class="lni-pencil-alt"></i>Definição de Rotas, Preços e suas Paragens</h3>
		</div>				
	</div>

	<div class="col-md-12">
		<div class="row">
			<div class="col-md-4">
				<div class="card">
					<h4 class="card-title" style="margin: 0"><i class="lni-microsoft"></i> Partida, Chegada e Preço</h4>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Or</b></span>
						<select class="form-control" id="province" name="departure">
							<option disabled selected>Origem</option>
							@foreach ($provinces as $province)
							<option value="{{$province->province_id}}">{{$province->name}}</option>
							@endforeach
							
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Ch</b></span>
						<select class="form-control" id="destiny" name="arrival">
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Pr</b></span>
						<input type="text" class="form-control" placeholder="Preço da Viagem" name="ticket_price">
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>ER.</b></span>
						<select class="form-control" id="province" name="route_state">
							<option disabled selected>Estado da Rota</option>
							<option value="Active">Operacional</option>
							<option value="Inactive">Inoperacional</option>
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Ca.</b></span>
						<select class="form-control" name="car">
							<option selected disabled>Selecionar Autocarro</option>
							@foreach ($buses as $bus)
							<option value="{{$bus->bus_id}}">{{$bus->license_plate}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			
			{{-- <div class="col-md-4">
				<div class="card">
					<h4 class="card-title" style="margin: 0"><i class="lni-producthunt"></i> Paragens</h4>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>P</b></span>
						<select class="form-control">
							<option disabled selected>Selecione a provincia</option>
							<option value="1">Maputo</option>
						</select>
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>N.P.</b></span>
						<input type="text" class="form-control" placeholder="Nome da paragem">
					</div>						
				</div>
			</div> --}}

			{{-- <div class="col-md-4">
				<div class="card">
					<h4 class="card-title" style="margin: 0"><i class="lni-coin"></i> Horarios</h4>
					<br>
					<div class="input-group">
						<select class="selectpicker" id="boot-select" multiple data-live-search="true">
							<option selected disabled>Selecionar dias</option>
							<option>Segunda-Feira</option>
							<option>Terça-Feira</option>
							<option>Quarta-Feira</option>
						</select>
					</div>
					
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>Hora de Partida</b></span>
						<input type="time" class="form-control" placeholder="Correio Electronico">
					</div>
				</div>
			</div> --}}

			<div class="col-md-12" style="margin-top: 1.2rem">
				<div class="card">
					<div class="input-group">
						<button type="reset" class="btn btn-warning">Limpar Campos</button>
						<button type="submit" class="btn btn-success float-right">Guardar</button>
					</div>
				</div>				
			</div>

		</div>
	</div>
</div>
</form>


<script>

	

</script>
@stop