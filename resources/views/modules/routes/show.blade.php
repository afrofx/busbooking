@extends('adminlte::page')

@section('title', 'Bus Booking')

@section('content_header')
    <h1>Administração</h1>
@stop

@section('content')
<div class="callout callout-info">
	<h4>Tip!</h4>

	<p>Add the fixed class to the body tag to get this layout. The fixed layout is your best option if your sidebar
		is bigger than your content because it prevents extra unwanted scrolling.</p>
</div>
@stop