@extends('adminlte::page')

@section('title', 'Paragens')

@section('content_header')
    <h1>Rotas e Suas Paragens</h1>
@stop

<link rel="stylesheet" href="../../css/table-style.css">

@section('content')

<div class="alert alert-info alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-info"></i> Informação!</h4>
	Esta página exibe todas as paragens das rotas
</div>

@if ($message = Session::get('success'))
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Alerta </h4>
		{{$message}}
	</div>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
		<h4>Erro ao submeter</h4><hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
	<div class="col-md-12">
		<form action="{{route('rotas.paragens.save')}}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="card">
				<h4 class="card-title" style="margin: 0"><i class="lni-producthunt"></i> Paragens</h4>
				<br>
				<div class="row">
					<div class="col-md-6">
						<div class="input-group">
							<span class="input-group-addon"><b>Ro</b></span>
							<select class="form-control" name="stoppage_route" required>
								<option disabled selected>Selecione a Rota</option>
								@foreach ($routes as $route)
									<option value="{{$route->route_id}}">
										@foreach ($provinces as $province)
											@if ($province->province_id == $route->from)
												{{$province->name}} para
											@endif
											@if ($province->province_id == $route->to)
											{{$province->name}}
											@endif
										@endforeach
									</option>
								@endforeach
							</select>
						</div>
						<br>
						<div class="input-group">
							<span class="input-group-addon"><b>P</b></span>
							<select class="form-control" name="stoppage_province" required>
								<option disabled selected>Selecione a provincia</option>
								@foreach ($provinces as $province)
									<option value="{{$province->province_id}}">{{$province->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="input-group">
							<span class="input-group-addon"><b>N.P.</b></span>
							<input type="text" name="stoppage_name" class="form-control" placeholder="Nome da paragem" required>
						</div>	
						<br>
						<div class="input-group">
							<button type="reset" class="btn btn-warning">Limpar Campos</button>
							<button type="submit" class="btn btn-success float-right">Guardar</button>
						</div>		
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="table-wrapper">
	<div class="table-title">
		<div class="row">
			<div class="col-sm-4">
				<h2><b>Paragens</b> das rotas</h2>
			</div>
			<div class="col-sm-8">						
				<a href="#" class="btn btn-primary"><i class="material-icons">&#xE863;</i> <span>Actualizar Lista</span></a>
				<a onclick="printStoppages()" class="btn btn-info"><i class="material-icons">&#xE24D;</i> <span>Imprimir Lista</span></a>
			</div>
		</div>
	</div>
		
	<table class="table table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Rota</th>
				<th>Paragem</th>
				<th>Provincia</th>
				<th>Adicionada à</th>
				<th>Acções</th>
			</tr>
		</thead>
		
		<tbody>
			@forelse ($stoppages as $stop )
				<tr>
					<td>{{$stop->stoppage_id}}</td>
					<td>
						@foreach ($provinces as $province)
							@if ($province->province_id == $stop->route->from)
								{{$province->name}}
							@endif
						@endforeach
						para
						@foreach ($provinces as $province)
							@if ($province->province_id == $stop->route->to)
								{{$province->name}}
							@endif
						@endforeach
					</td>
					<td>{{$stop->stop_name}}</td>
					<td>{{$stop->province->name}}</td>
					<td>{{$stop->created_at}}</td>
					<td>
						{!! Form::open(['method' => 'DELETE','route' => ['rotas.paragens.destroy', $stop->stoppage_id],'style'=>'display:inline']) !!}
							<button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}
					</td>
				</tr>
			@empty
				<tr>
					Sem Rotas Registadas
				</tr>
			@endforelse
		</tbody>
	</table>
</div>



<table id="table-print" class="table-print" hidden>
	<thead>
		<tr style="background: rgb(231, 76, 60); color: white;">
			<th>##</th>
			<th>Rota</th>
			<th>Paragem</th>
			<th>Provincia</th>
			<th>Adicionada à</th>
		</tr>
	</thead>
	<tbody>
		@forelse ($stoppages as $stop )
			<tr>
				<td>{{$stop->stoppage_id}}</td>
				<td>
					@foreach ($provinces as $province) @if ($province->province_id == $stop->route->from) {{$province->name}} @endif @endforeach para @foreach ($provinces as $province) @if ($province->province_id == $stop->route->to) {{$province->name}} @endif @endforeach
				</td>
				<td>{{$stop->stop_name}}</td>
				<td>{{$stop->province->name}}</td>
				<td>{{$stop->created_at}}</td>
			</tr>
		@empty
			<tr>
				Sem Paragens Registadas
			</tr>
		@endforelse
	</tbody>
</table>
@stop