@extends('layouts.root')

@section('title', 'Destinos')


@section('content')

<style>


.order-card {
    color: #fff;
}


.bg-c-green {
    background: linear-gradient(45deg,#2ed8b6,#59e0c5);
}

.bg-c-yellow {
    background: linear-gradient(45deg,#FFB64D,#ffcb80);
}

.bg-c-pink {
    background: linear-gradient(45deg,#FF5370,#ff869a);
}


.card {
    border-radius: 5px;
    -webkit-box-shadow: 0 1px 2.94px 0.06px rgba(4,26,55,0.16);
    box-shadow: 0 1px 2.94px 0.06px rgba(4,26,55,0.16);
    border: none;
    margin-bottom: 30px;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
}

.card .card-block {
    padding: 6px;
}

.order-card i {
    font-size: 26px;
}

.f-left {
    float: left;
}

.f-right {
    float: right;
}

.card{
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    border: 0;
    color: #212121
}

.card-text{
    font-size: 12pt
}

.progress, .progress .progress-bar {
    height: 6px;
}
</style>


<div class="home-section container">
        <a class="weatherwidget-io" href="https://forecast7.com/pt/n25d9732d57/maputo/" data-label_1="MAPUTO" data-label_2="TEMPERATURA" data-theme="pure" >MAPUTO TEMPERATURA</a>
        <script>
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
        </script>
    <div class="province mt-3">
        <div class="row">
            <div class="col-md-6">
                <p class="text-white text-xl">10 Provincias, <span class="text-warning">{{$routes->count()}} Rotas</span></p>
            </div>
            <div class="col-md-6">
                <button class="btn btn-warning"><a href="/" target="" class="link-null">Reserve Já</a></button>
            </div>
        </div>
    </div>

    <div class="mt-3">
        <div class="row">
            @foreach ($routes as $route)
            <div class="col-md-4 col-xl-3">
                <div class="card bg-c-blue order-card">
                    <div class="card-block">
                        <h2 class="text-right"><img class="f-left" src="../images/bus-stop.png" alt="" width="48px"><span>{{$route->price}}</span></h2>
                        <br>
                        <p class="card-text mb-0">Cadeiras Disponíveis<span class="f-right">54</span></p>
                        <div class="progress my-2">
                            <div class="progress-bar red accent-2" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <p class="card-text">
                            @foreach ($provinces as $province)
                                @if ($province->province_id == $route->from)
                                    <b>{{$province->name}}</b> para
                                @endif
                                @if ($province->province_id == $route->to)
                                    <b>{{$province->name}}</b>
                                @endif
                            @endforeach
                        </p>
                    </div>
                </div> 
            </div>
            @endforeach
            {{-- <div class="col-md-6">
                <table class="table gradient-table">
                    <thead class="gradient-table">
                        <tr>
                            <th class="py-0" colspan="2">Origem</th>
                            <th class="py-0">Destino</th>
                            <th class="py-0">Preço</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($routes as $route)
                        <tr>
                                @foreach ($provinces as $province)
                                    @if ($province->province_id == $route->from)
                                        <td class="py-0">{{$province->name}}</td>
                                        <td class="py-0"><b><i class="lni-arrow-right"></i></b></td>
                                    @endif
                                    @if ($province->province_id == $route->to)
                                        <td class="py-0">{{$province->name}}</td>
                                    @endif
                                @endforeach
                                
                            <td class="py-0">{{$route->price}} MZN</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> --}}
        </div>
    </div>
</div>

@stop