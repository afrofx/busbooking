@extends('adminlte::page')

@section('title', 'Rotas')

@section('content_header')
    <h1>Rotas Registadas</h1>
@stop

<link rel="stylesheet" href="../css/table-style.css">

@section('content')
<div class="alert alert-info alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-info"></i> Informação!</h4>
	Esta página exibe todas as rotas registadas no sistema
</div>

@if ($message = Session::get('success'))
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Alerta </h4>
		{{$message}}
	</div>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
		<h4>Erro ao submeter</h4><hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
	<div class="col-md-12">
	<form action="{{route('rotas.save')}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="card">
			<h4 class="card-title" style="margin: 0"><i class="lni-microsoft"></i> Partida, Chegada e Preço</h4>
			<br>
			<div class="row">
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><b>Or</b></span>
						<select class="form-control" id="province" name="departure" required>
							<option disabled selected hidden value="">Origem</option>
							@foreach ($provinces as $province)
								<option value="{{$province->province_id}}">{{$province->name}}</option>
							@endforeach
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Ch</b></span>
						<select class="form-control" id="destiny" name="arrival" required>
							<option disabled selected hidden value="">Chegada</option>
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Pr</b></span>
						<input type="text" class="form-control" placeholder="Preço da Viagem" name="ticket_price" required>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><b>ER.</b></span>
						<select class="form-control" id="province" name="route_state" required>
							<option disabled selected value="" hidden>Estado da Rota</option>
							<option value="Active">Operacional</option>
							<option value="Inactive">Inoperacional</option>
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Ca.</b></span>
						<select class="form-control" name="car" required>
							<option selected disabled value="" hidden>Selecionar Autocarro</option>
							@foreach ($buses as $bus)
							<option value="{{$bus->bus_id}}">{{$bus->license_plate}}</option>
							@endforeach
						</select>
					</div>
					<br>
					<div class="input-group">
						<button type="reset" class="btn btn-warning">Limpar Campos</button>
						<button type="submit" class="btn btn-success float-right">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>

<div class="table-wrapper">
	<div class="table-title">
		<div class="row">
			<div class="col-sm-4">
				<h2><b>Rotas</b> Registadas no sistema</h2>
			</div>
			<div class="col-sm-8">						
				<a href="#" class="btn btn-primary"><i class="material-icons">&#xE863;</i> <span>Actualizar Lista</span></a>
				<a onclick="printRoutes()" class="btn btn-info"><i class="material-icons">&#xE24D;</i> <span>Imprimir Lista</span></a>
			</div>
		</div>
	</div>
		
	<table class="table table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Origem</th>
				<th>Destino</th>
				<th>Preço</th>
				<th>Estado</th>
				<th>Automovel</th>
				<th>Registada à</th>
				<th>Acções</th>
			</tr>
		</thead>
		
		<tbody>
			@forelse ($routes as $route )
				<tr>
					<td>{{$route->route_id}}</td>
					@foreach ($provinces as $province)
						@if ($province->province_id == $route->from)
							<td>{{$province->name}}</td>
						@endif
					@endforeach
					@foreach ($provinces as $province)
						@if ($province->province_id == $route->to)
							<td>{{$province->name}}</td>
						@endif
					@endforeach
					<td>{{$route->price}}</td>
					<td>
						@if ($route->state == 'Active')
							<span class="label label-success">Disponivel</span>	
						@else
							<span class="label label-danger">Indisponivel</span>
						@endif
						
					</td>
					<td>{{$route->buses->license_plate}}</td>
					<td>{{$route->created_at}}</td>
					<td>
						{!! Form::open(['method' => 'DELETE','route' => ['rotas.destroy', $route->route_id],'style'=>'display:inline']) !!}
							<button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}
					</td>
				</tr>
			@empty
				<tr>
					Sem Rotas Registadas
				</tr>
			@endforelse
		</tbody>
	</table>
</div>



<table id="table-print" class="table-print" style="display: none">
	<thead>
		<tr style="background: rgb(231, 76, 60); color: white;">
			<th>##</th>
			<th>Origem</th>
			<th>Destino</th>
			<th>Preço</th>
			<th>Estado</th>
			<th>Automovel</th>
			<th>Registada à</th>
		</tr>
	</thead>
	<tbody>
		@forelse ($routes as $route )
			<tr>
				<td>{{$route->route_id}}</td>
				@foreach ($provinces as $province)
					@if ($province->province_id == $route->from)
						<td>{{$province->name}}</td>
					@endif
				@endforeach
				@foreach ($provinces as $province)
					@if ($province->province_id == $route->to)
						<td>{{$province->name}}</td>
					@endif
				@endforeach
				<td>{{$route->price}}</td>
				<td>
					@if ($route->state == 'Active')
						<span class="label label-success">Disponivel</span>	
					@else
						<span class="label label-danger">Indisponivel</span>
					@endif
					
				</td>
				<td>{{$route->buses->license_plate}}</td>
				<td>{{$route->created_at}}</td>
			</tr>
			@empty
			<tr>
				Sem Rotas Registadas
			</tr>
		@endforelse
	</tbody>
</table>
@stop