@extends('adminlte::page')

@section('title', 'Adicionar Rota')

@section('content_header')
    <h1>Adicionar Rota</h1>
@stop

@section('content')
<div class="callout callout-info">
	<h4>Informação!</h4>
	<p>Adicione novas rotas e aloque autocarros as mesmas</p>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
		<h4>Erro ao submeter</h4><hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<style>
	h3{
		margin-top: 0%;
	}
	
</style>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 1.2rem">
		<div class="card">
			<h3 class="card-title" style="margin: 0"><i class="lni-pencil-alt"></i>Definição de Rotas, Preços e suas Paragens</h3>
		</div>				
	</div>

	<div class="col-md-12">
		<div class="row">
			<form action="../../admin/destinos" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
			<div class="col-md-4">
				<div class="card">
					<h4 class="card-title" style="margin: 0"><i class="lni-microsoft"></i> Partida, Chegada e Preço</h4>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Or</b></span>
						<select class="form-control" id="province" name="departure">
							<option disabled selected>Origem</option>
							@foreach ($provinces as $province)
								<option value="{{$province->province_id}}">{{$province->name}}</option>
							@endforeach
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Ch</b></span>
						<select class="form-control" id="destiny" name="arrival">
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Pr</b></span>
						<input type="text" class="form-control" placeholder="Preço da Viagem" name="ticket_price">
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>ER.</b></span>
						<select class="form-control" id="province" name="route_state">
							<option disabled selected>Estado da Rota</option>
							<option value="Active">Operacional</option>
							<option value="Inactive">Inoperacional</option>
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Ca.</b></span>
						<select class="form-control" name="car">
							<option selected disabled>Selecionar Autocarro</option>
							@foreach ($buses as $bus)
							<option value="{{$bus->bus_id}}">{{$bus->license_plate}}</option>
							@endforeach
						</select>
					</div>
					<br>
					<div class="input-group">
						<button type="reset" class="btn btn-warning">Limpar Campos</button>
						<button type="submit" class="btn btn-success float-right">Guardar</button>
					</div>
				</div>
			</div>
			</form>
			<form action="{{ action('DestinyController@addStoppage')}}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
			<div class="col-md-4">
				<div class="card">
					<h4 class="card-title" style="margin: 0"><i class="lni-producthunt"></i> Paragens</h4>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Ro</b></span>
						<select class="form-control" name="stoppage_route">
							<option disabled selected>Selecione a Rota</option>
							@foreach ($routes as $route)
								<option value="{{$route->route_id}}">
									@foreach ($provinces as $province)
										@if ($province->province_id == $route->from)
											{{$province->name}} para
										@endif
										@if ($province->province_id == $route->to)
										{{$province->name}}
										@endif
									@endforeach
								</option>
							@endforeach
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>P</b></span>
						<select class="form-control" name="stoppage_province">
							<option disabled selected>Selecione a provincia</option>
							@foreach ($provinces as $province)
								<option value="{{$province->province_id}}">{{$province->name}}</option>
							@endforeach
						</select>
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>N.P.</b></span>
						<input type="text" name="stoppage_name" class="form-control" placeholder="Nome da paragem">
					</div>	
					<br>
					<div class="input-group">
						<button type="reset" class="btn btn-warning">Limpar Campos</button>
						<button type="submit" class="btn btn-success float-right">Guardar</button>
					</div>					
				</div>
			</div>
		</form>

		<form action="{{ action('DestinyController@addTimes')}}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="col-md-4">
				<div class="card">
					<h4 class="card-title" style="margin: 0"><i class="lni-coin"></i> Horarios</h4>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Ro</b></span>
						<select class="form-control" name="departure_route">
							<option disabled selected>Selecione a Rota</option>
							@foreach ($routes as $route)
								<option value="{{$route->route_id}}">
									@foreach ($provinces as $province)
										@if ($province->province_id == $route->from)
											{{$province->name}} para
										@endif
										@if ($province->province_id == $route->to)
										{{$province->name}}
										@endif
									@endforeach
								</option>
							@endforeach
						</select>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><b>Dia</b></span>
						<select class="form-control" name="departure_day">
							<option selected disabled>Selecionar dias</option>
							@foreach ($route_days as $route_day)
								<option value="{{$route_day->route_day_id}}">{{$route_day->route_day}}</option>
							@endforeach
						</select>
					</div>
					{{-- <select class="selectpicker" id="boot-select" multiple data-live-search="true">
							<option selected disabled>Selecionar dias</option>
							@foreach ($route_days as $route_day)
								<option value="{{$route_day->route_day_id}}">{{$route_day->route_day}}</option>
							@endforeach
						</select> --}}
					<br>

					<div class="input-group">
						<span class="input-group-addon"><b>Hora de Partida</b></span>
						<input type="time" name="departure_time" class="form-control" placeholder="Correio Electronico">
					</div>
					<br>
					
					<div class="input-group">
						<button type="reset" class="btn btn-warning">Limpar Campos</button>
						<button type="submit" class="btn btn-success float-right">Guardar</button>
					</div>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>


<script>

	

</script>
@stop