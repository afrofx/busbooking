@extends('adminlte::page')

@section('title', 'Adicionar autocarro')

@section('content_header')
    <h1>Editar Autocarro</h1>
@stop

@section('content')

<style>
h3{
	margin-top: 0%;
}

</style>
<div class="callout callout-info">
	<h4>Informação!</h4>
	<p>Este campo é dedicado a edição de dados dos autocarros, assim como a modificação dos estados dos mesmos</p>
</div>


@if ($errors->any())
    <div class="alert alert-danger">
		<h4>Erro ao submeter</h4><hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<form action="{{route('autocarros.update', $bus->bus_id)}}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<h3 class="card-title m-0 "><i class="lni-pencil-alt"></i>Detalhes do Autocarro</h3>
			<div class="row">
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>R</b></span>
						<input name="reference" type="text" value="{{$bus->reference}}" class="form-control" placeholder="Referência do Autocarro" required>
					</div>
					
					<br>
					
					<div class="input-group">
						<span class="input-group-addon"><b>M</b></span>
						<input name="car_brand" type="text"  value="{{$bus->brand}}" class="form-control" placeholder="Marca do Autocarro" required>
					</div>
					
					<br>
					
					<div class="input-group">
						<span class="input-group-addon"><b>M</b></span>
						<input name="car_model" type="text"  value="{{$bus->model}}" class="form-control" placeholder="Modelo do Autocarro" required>
					</div>

					<br>

					<div class="input-group">
						<span class="input-group-addon"><b><i class="lni-calendar"></i></b></span>
						<input type="text" name="data_registo" value="{{$bus->register_date}}" class="form-control datepicker" required>
					</div>

				</div>
				
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><i class="lni-text-size"></i></span>
						<input type="text" class="form-control" value="{{$bus->license_plate}}" name="license_plate" placeholder="Matricula" required>
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-leaf"></i></span>
						<select class="form-control" name="car_fuel_type" required>
							<option disabled selected>Tipo de Combustivel</option>
							<option value="Gasolina" @if ($bus->fuel_type == "Gasolina") selected @endif>Gasolina</option>
							<option value="Diesel" @if ($bus->fuel_type == "Diesel") selected @endif>Diesel</option>
							<option value="Hibrido" @if ($bus->fuel_type == "Hibrido") selected @endif>Hibrido</option>
						</select>
					</div>
					
					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-scroll-down"></i></span>
						<select class="form-control" name="car_transmission" required>
							<option disabled selected hidden value="">Tipo de Transmissão</option>
							<option value="Manual" @if ($bus->transmission == "Manual") selected @endif>Manual</option>
							<option value="Automatica" @if ($bus->transmission == "Automatica") selected @endif>Automatica</option>
						</select>
					</div>

					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-text-size"></i></span>
						<input type="text" class="form-control"  value="{{$bus->seat_number}}" name="car_seat_number" placeholder="Número de cadeiras" required>
					</div>
				</div>

				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>Imagem</b></span>
						<input type="file" name="car_photo" accept="image/jpeg, image/png, image/gif" class="form-control" placeholder="Imagens do Autocarro">
					</div>
					
					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-more"></i></span>
						<select class="form-control" name="car_state" required>
							<option disabled selected hidden value="">Estado do Veículo</option>
							<option value="Disponivel" @if ($bus->bus_state == "Disponivel") selected @endif>Disponivel</option>
							<option value="Indisponivel"  @if ($bus->bus_state == "Indisponivel") selected @endif>Indisponivel</option>
						</select>
					</div>

					<br>
					<div class="input-group">
						<button type="submit" class="btn btn-success float-right">Actualizar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
@stop


