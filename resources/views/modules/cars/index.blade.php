@extends('adminlte::page')

@section('title', 'Lista de autocarros')

@section('content_header')
    <h1>Autocarros Registados</h1>
@stop

<link rel="stylesheet" href="../css/table-style.css">

@section('content')
<div class="alert alert-info alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-info"></i> Informação!</h4>
	Encontre a lista de autocarros registados na base de dados
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-check"></i> Alerta </h4>
	{{$message}}
</div>
@endif


<div class="table-wrapper">
	<div class="table-title">
		<div class="row">
			<div class="col-sm-4">
				<h2><b>Carros</b> Registados no Sistema</h2>
			</div>
			<div class="col-sm-8">						
				<a href="#" class="btn btn-primary"><i class="material-icons">&#xE863;</i> <span>Actualizar Lista</span></a>
				<a onclick="printCars()" class="btn btn-info"><i class="material-icons">&#xE24D;</i> <span>Imprimir Lista</span></a>
			</div>
		</div>
	</div>
	<table id="dataTable" class="table table-striped table-hover display">
		<thead>
			<tr>
				<th>Referência</th>
				<th>Marca</th>
				<th>Modelo</th>
				<th>Matricula</th>
				<th>Estado</th>
				<th>Operação</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($buses as $bus)
			<tr>
				<td>{{$bus->reference}}</td>
				<td>{{$bus->brand}}</td>
				<td>{{$bus->model}}</td>
				<td>{{$bus->license_plate}}</td>
				@if ($bus->bus_state == "Disponivel")
					<td><span class="label label-success">{{$bus->bus_state}}</span></td>
				@else
					<td><span class="label label-danger">{{$bus->bus_state}}</span></td>
				@endif
				<td>
					<a href="{{ route('autocarros.show', $bus->bus_id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
					<a href="{{ route('autocarros.edit', $bus->bus_id) }}" class="btn btn-sm btn-warning text-white"><i class="fa fa-pen"></i></a>
					{!! Form::open(['method' => 'DELETE','route' => ['autocarros.destroy', $bus->bus_id],'style'=>'display:inline']) !!}
						<button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
					{!! Form::close() !!}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>



<table id="table-print" class="table-print" style="display: none">
	<thead>
		<tr style="background: rgb(231, 76, 60); color: white;">
			<th>Referência</th>
			<th>Marca</th>
			<th>Modelo</th>
			<th>Matricula</th>
			<th>Estado</th>
			<th>Data de Registo</th>
		</tr>
	</thead>
	<tbody>
			@foreach ($buses as $bus)
				<tr>
					<td>{{$bus->reference}}</td>
					<td>{{$bus->brand}}</td>
					<td>{{$bus->model}}</td>
					<td>{{$bus->license_plate}}</td>
					@if ($bus->bus_state == "Disponivel")
						<td style="color: green">{{$bus->bus_state}}</td>
					@else
						<td style="color: red">{{$bus->bus_state}}</td>
					@endif
					<td>{{$bus->created_at}}</span></td>
				</tr>
			@endforeach
	</tbody>
</table>
@stop