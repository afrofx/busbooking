@extends('adminlte::page')

@section('title', 'Bus Booking')

@section('content_header')
    <h1>Administração</h1>
@stop

@section('content')
<div class="callout callout-info">
	<h4>Informação</h4>
	<p>Detalhes do autocarro {{$bus->brand}} - {{$bus->license_plate}}</p>
</div>


<div class="row">
	<div class="col-sm-12 col-md-5">
		<div class="box box-solid">
			<div class="box-body">
				<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
					{{$bus->brand}} - {{$bus->license_plate}}
				</h4>
				<div class="media">
					<div class="media-left">
						<a href="#" class="ad-click-event">
							<img src="../../storage/images/cars/{{$bus->photo}}" class="img-responsive">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<div class="col-sm-12 col-md-7">
		<div class="box box-solid">
			<div class="box-body">
				<h4 style="background-color:#f7f7f7; font-size: 18px; padding: 7px 10px; margin-top: 0;">
					<b>Detalhes Adicionais</b>
				</h4>
				<div class="media">
					<div class="media-body">
						<style>
							hr{
								margin:0px !important;
							}
						</style>
						<div class="clearfix">
							<h4 style="margin-top: 0"><b>Marca:</b> <span class="pull-right">{{$bus->brand}}</span></h4><hr >
							<h4 style="margin-top: 0"><b>Modelo:</b> <span class="pull-right">{{$bus->model}}</span></h4><hr >
							<h4 style="margin-top: 0"><b>Matricula:</b> <span class="pull-right">{{$bus->license_plate}}</span></h4><hr >
							<h4 style="margin-top: 0"><b>Referencia:</b> <span class="pull-right">{{$bus->reference}}</span></h4><hr >
							<h4 style="margin-top: 0"><b>Numero de cadeiras:</b> <span class="pull-right">{{$bus->seat_number}}</span></h4><hr >
							<h4 style="margin-top: 0"><b>Combustivel:</b> <span class="pull-right">{{$bus->fuel_type}}</span></h4><hr >
							<h4 style="margin-top: 0"><b>Transmissão:</b> <span class="pull-right">{{$bus->transmission}}</span></h4><hr >
							<h4 style="margin-top: 0"><b>Estado:</b> <span class="pull-right">{{$bus->bus_state}}</span></h4><hr >
							<h4 style="margin-top: 0"><b>Data de Registo:</b> <span class="pull-right">{{$bus->register_date}}</span></h4><hr >
							<h4 style="margin-top: 0"><b>Ultima Actualização:</b> <span class="pull-right">{{$bus->updated_at}}</span></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop