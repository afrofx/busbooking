@extends('adminlte::page')

@section('title', 'Adicionar autocarro')

@section('content_header')
    <h1>Adicionar novo Autocarro</h1>
@stop

@section('content')

<style>
h3{
	margin-top: 0%;
}

</style>
<div class="callout callout-info">
	<h4>Informação!</h4>
	<p>Este campo é dedicado ao registo de novos autocarros, assim como a modificação dos estados dos mesmos</p>
</div>


@if ($errors->any())
    <div class="alert alert-danger">
		<h4>Erro ao submeter</h4><hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<form action="{{route('autocarros.store')}}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<h3 class="card-title m-0 "><i class="lni-pencil-alt"></i>Detalhes do Autocarro</h3>
			<div class="row">
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>R</b></span>
						<input name="reference" type="text" class="form-control" placeholder="Referência do Autocarro" required>
					</div>
					
					<br>
					
					<div class="input-group">
						<span class="input-group-addon"><b>M</b></span>
						<input name="car_brand" type="text" class="form-control" placeholder="Marca do Autocarro" required>
					</div>
					
					<br>
					
					<div class="input-group">
						<span class="input-group-addon"><b>M</b></span>
						<input name="car_model" type="text" class="form-control" placeholder="Modelo do Autocarro" required>
					</div>

					<br>

					<div class="input-group">
						<span class="input-group-addon"><b><i class="lni-calendar"></i></b></span>
						<input type="text" name="data-registo" class="form-control datepicker" placeholder="Data de Registo" required>
					</div>

				</div>
				
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><i class="lni-text-size"></i></span>
						<input type="text" class="form-control" name="license_plate" placeholder="Matricula" required>
					</div>
					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-leaf"></i></span>
						<select class="form-control" name="car_fuel_type">
							<option disabled selected>Tipo de Combustivel</option>
							<option value="Gasolina">Gasolina</option>
							<option value="Diesel">Diesel</option>
							<option value="Hibrido">Hibrido</option>
						</select>
					</div>
					
					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-scroll-down"></i></span>
						<select class="form-control" name="car_transmission">
							<option disabled selected>Tipo de Transmissão</option>
							<option value="Manual">Manual</option>
							<option value="Automatica">Automatica</option>
						</select>
					</div>

					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-text-size"></i></span>
						<input type="text" class="form-control" name="car_seat_number" placeholder="Número de cadeiras" value="75" required>
					</div>
				</div>

				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><b>Imagens</b></span>
						<input type="file" name="car_photo" accept="image/jpeg, image/png, image/gif" class="form-control" placeholder="Imagens do Autocarro">
					</div>
					
					<br>

					<div class="input-group">
						<span class="input-group-addon"><i class="lni-more"></i></span>
						<select class="form-control" name="car_state" required>
							<option disabled selected>Estado do Veículo</option>
							<option value="Disponivel">Disponivel</option>
							<option value="Indisponivel">Indisponivel</option>
						</select>
					</div>

					<br>
					<div class="input-group">
						<button type="reset" class="btn btn-warning">Limpar Campos</button>
						<button type="submit" class="btn btn-success float-right">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
@stop