@extends('layouts.root')

@section('title', 'ETRAGO')


@section('content')


<div class="home-section container">
        <a class="weatherwidget-io" href="https://forecast7.com/pt/n25d9732d57/maputo/" data-label_1="MAPUTO" data-label_2="TEMPERATURA" data-theme="pure" >MAPUTO TEMPERATURA</a>
        <script>
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
        </script>

<style>

#holder{    
height:240px;    
width:720px;
background-color:#F5F5F5;
border:1px solid #A4A4A4;
margin: 0 auto;
text-align: center;

}

.taked{ 
    background:url('../images/booked_seat_img.gif') no-repeat !important; 
    padding-left:35px !important; 
    padding-right:2px !important; 
    font-weight: bolder !important;
}
.free  { 
    background:url('../images/available_seat_img.gif') no-repeat; 
    padding-left:35px; 
    padding-right:2px; 
    font-weight: bolder; 
}
.selec { 
    background:url('../images/selected_seat_img.gif') no-repeat; 
    padding-left:35px; 
    padding-right:2px; 
    font-weight: bolder; 
}
#place {
position:relative;
margin:7px;
}
#place a{
font-size:0.6em;
}
#place li
{
 list-style: none outside none;
 position: absolute;   
}    
#place .row-3, #place .row-4{
margin-top:10px;
}


#seatDescription{
    margin: 0 auto;
    text-align: center;
}
#seatDescription li{
verticle-align:middle;    
list-style: none outside none;
padding-left:35px;
height:35px;
float:left;
}

#place a{
    font-size: 0.7rem;
    font-weight: bold;
    margin-left: -30px;
    color:black !important;
}
</style>

<form action="{{ action('HomeController@storePassenger')}}" method="POST" enctype="multipart/form-data">
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="soft-shadow bg-white p-4">
                    {{ csrf_field() }}
                    <p>Dados do Passageiro <hr></p>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name"><b>Primeiro Nome:</b></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Ex: Maria" required>
                            </div>
                            <div class="form-group">
                                <label for="surname"><b>Apelido</b></label>
                                <input type="text" class="form-control" id="surname" name="surname" placeholder="Ex: Mabote" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="seat"><b>Selecionar Cadeira</b></label>
                                <select class="form-control" name="seat" required>
                                    <option selected hidden disabled value="">Selecione a cadeira</option>
                                    @for ($i = 1; $i < 72; $i++)
                                        <option value="{{$i}}">Cadeira {{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="id-number"><b>Bilhete de Edentidade</b></label>
                                <input type="text" class="form-control" id="id-number" name="id-number" placeholder="Ex: 111002552255B" required>
                            </div>
                            <div class="form-group">
                                <label for="birthday"><b>Data de Nascimento</b></label>
                                <input type="date" class="form-control" name="birthday" id="birthday" placeholder="Data de Nascimento" required>
                            </div>
                            <div class="form-group">
                                <label for="contact"><b>Contacto</b></label>
                                <input type="text" class="form-control" name="contact" id="contact" placeholder="Ex: 84 00 22 001" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="parent"><b>Contacto do Parente</b></label>
                                <input type="text" class="form-control" name="parent" id="parent" placeholder="Ex: 84 00 22 001" required>
                            </div>
                            <div class="form-group">
                                <label for="email"><b>Correio electronico</b></label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Ex: mabote@exemplo.co.mz">
                            </div>
                            <button type="submit" class="btn btn-primary">Concluir</button>
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-12">
            <h2> Escolha o lugar onde deseja sentar-se:</h2>
            <div class="row">
                <div class="col-md-6 float-right">
                    <p><b>
                    @if ($value['book_type']=='Single')
                        Apenas IDA:
                    @else 
                        IDA e Volta:
                    @endif</b>  {{$partida[0]->name}} para {{$destino[0]->name}}</p><hr>
                    <p><b>Numero de passageiros:</b> {{$value['passenger_numb']}} </p>
                </div>

                <div class="col-md-6 pull-left">
                    <p><b>Data e Hora:</b> {{$value['departure_date'] }} </p>
                    <hr>
                    <p><b>Preço:</b> {{ number_format((float)$preco[0]->price * $value['passenger_numb'], 2, '.', '')}} </p>
                </div>
            </div>
            <div class="row text-right"> 
                <div class="col-md-10">
                    <table class="table">
                        <tbody>
                          <tr>
                            <td class="p-1"><input type="button" value="01" class="btn btn-secondary free text-bold text-dark @foreach($reservations as $res) @if($res->seat == "1") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="02" class="btn btn-secondary free text-bold text-dark @foreach($reservations as $res) @if($res->seat == "2") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="03" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "3") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="04" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "4") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="05" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "5") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="06" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "6") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="07" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "7") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="08" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "8") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="09" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "9") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="10" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "10") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="11" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "11") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="12" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "12") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="13" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "13") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="14" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "14") taked @endif @endforeach"/></td>
                          </tr>
                          
                          <tr>
                            <td class="p-1"><input type="button" value="15" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "15") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="16" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "16") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="17" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "17") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="18" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "18") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="19" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "19") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="20" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "20") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="21" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "21") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="22" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "22") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="23" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "23") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="24" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "24") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="25" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "25") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="26" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "26") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="27" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "27") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="28" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "28") taked @endif @endforeach"/></td>
                          </tr>

                          
                          <tr>
                            <td class="p-1"><input type="button" value="29" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "29") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="30" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "30") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="31" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "31") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="32" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "32") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="33" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "33") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="34" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "34") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="35" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "35") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="36" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "36") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="37" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "37") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="38" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "38") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="39" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "39") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="40" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "40") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="41" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "41") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="42" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "42") taked @endif @endforeach"/></td>
                          </tr>
                          
                            <tr>
                                <td class="p-1" colspan="15"><input type="button" value="43" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "43") taked @endif @endforeach"/></td>
                            </tr>
                          
                          <tr>
                            <td class="p-1"><input type="button" value="44" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "44") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="45" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "45") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="46" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "46") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="47" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "47") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="48" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "48") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="49" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "49") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="50" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "50") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="51" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "51") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="52" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "52") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="53" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "53") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="54" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "54") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="55" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "55") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="56" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "56") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="57" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "57") taked @endif @endforeach"/></td>
                          </tr>
                          
                          <tr>
                            <td class="p-1"><input type="button" value="58" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "58") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="59" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "59") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="60" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "60") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="61" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "61") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="62" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "62") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="63" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "63") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="64" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "64") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="65" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "65") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="66" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "66") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="67" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "67") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="68" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "68") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="69" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "69") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="70" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "70") taked @endif @endforeach"/></td>
                            <td class="p-1"><input type="button" value="71" class="btn btn-secondary text-bold text-dark free @foreach($reservations as $res) @if($res->seat == "71") taked @endif @endforeach"/></td>
                          </tr>
                        </tbody>
                      </table>
                </div> 
            </div>
            <div class="row mt-4"> 
                <ul id="seatDescription">
                    <li style="background:url('../images/available_seat_img.gif') no-repeat scroll 0 0 transparent;">Cadeiras Disponiveis</li>
                    <li style="background:url('../images/booked_seat_img.gif') no-repeat scroll 0 0 transparent;">Cadeiras Reservadas</li>
                    <li style="background:url('../images/selected_seat_img.gif') no-repeat scroll 0 0 transparent;">Cadeiras Selecionadas</li>
                </ul>
            </div>
            <div class="mt-3" style="clear:both;width:100%">         
            </div>
            <input type="hidden" id="passNumber" value="{{$value['passenger_numb']}}"/>
        </div>
    </div>
</div>

                </form>
@stop