@extends('layouts.root')

@section('title', 'ETRAGO')

@section('content')

<div class="home-section container">

        <a class="weatherwidget-io" href="https://forecast7.com/pt/n25d9732d57/maputo/" data-label_1="MAPUTO" data-label_2="TEMPERATURA" data-theme="pure" >MAPUTO TEMPERATURA</a>
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

        <div class="row">
            <div class="col-md-12">
                <table class="table tab" >
                    <thead>
                        <tr class="soft-shadow">
                            <th class="py-0 border-0"></th>
                            <th class="py-0 border-0" colspan="2">Origem</th>
                            <th class="py-0 border-0">Destino</th>
                            <th class="py-0 border-0">Data de partida</th>
                            <th class="py-0 border-0">Preço & Hora</th>
                            <th class="py-0 border-0">Paragem</th>
                            <th class="py-0 border-0">Operação</th>
                        </tr>
                    </thead>
                    <tbody class="m-4">
                        @foreach ($routes as $route)
                        <tr class="soft-shadow">
                                <td class="py-0 border-0 p-2"> <img width="32px" src="/images/route.png" alt="icone"></td>
                                @foreach ($provinces as $province)
                                    @if ($province->province_id == $route->from)
                                        <td class="py-0 border-0">Provincia de <br>{{$province->name}}</td>
                                        <td class="py-0 border-0"><b><i class="lni-arrow-right"></i></b></td>
                                    @endif
                                    @if ($province->province_id == $route->to)
                                        <td class="py-0 border-0">Provincia de <br>{{$province->name}}</td>
                                    @endif
                                @endforeach
                            
                            <td class="py-0 border-0">{{$value}} <br> {{$day_week}}</td>    
                            <td class="py-0 border-0">{{$route->price}} MZN <br> {{$route->departure_time}}</td>
                            <td class="py-0 border-0 pt-2"> {{$route->stop_name}}</td>
                            <td class="py-0 border-0"> <a class="btn btn-sm btn-success mt-2 border-0 text-white" type="submit" href="passagens/reservar">Reservar Já</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            <div class="col-md-4">
                
            </div>
        </div>
</div>

@stop