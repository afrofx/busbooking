@extends('layouts.root')

@section('title', 'ETRAGO')

@section('content')



<div class="container">
    <div class="bg-white px-5 py-2 mt-3">
		<h4>Mais operações</h4>
		<a class="btn btn-sm btn-warning text-dark" onclick="printTicket()">Imprimir</a>
		<a href="/" class="btn btn-sm btn-danger text-white">Cancelar</a>
		<button class="btn btn-sm btn-success text-white" data-toggle="modal" data-target="#exampleModal">Efectuar Pagamento</button> <small>Sistemas de pagamentos online indisponiveis(Diriga-se a agencia)</small> 
    </div>
    <div class="bg-white p-5 mt-2">

      	<span class="text-center text-white m-0 bg-danger p-2 font-weight-bold" >Bilhete não pago</span>
		{{-- <span class="text-center text-white m-0 bg-success p-2 font-weight-bold" >Bilhete pago</span> --}}
		  
       	<div class="row pt-3 mb-2">
        	<div class="col-md-6 pull-left">
				<img src="../assets/img/Etrago-Lda.png" width="160px" class="img-responsive">
			</div>
        	<div class="col-md-6 text-right">
          		<h5 class="pt-4">Ref. <span class="text-danger">{{$results[0]->reference}}</span></h5>
         		 <p class="text-muted mb-0"><i>Data: {{$results[0]->created_at}}</i></p>
        	</div>
		</div>
		  
		<div class="row b-t pt-5">
			<div class="col-md-6 center">
				<h5>Informação do passageiro</h5>
				<p><b>Nome: </b>{{$results[0]->name}} {{$results[0]->surname}}</p>
				<p><b>Bilhete de Identidade: </b>{{$results[0]->id_number}}</p>
				<p><b>Contacto: </b>{{$results[0]->contact}} | {{$results[0]->contact}} (Familiar)</p>
			</div>
			<div class="col-md-6 text-right">
				<h5>Detalhes de Pagamento</h5>
				<p>Entidade: 987748</p>
				<p>Referência: 0000324432</p>
				<p>Canais de Pagamento: M-PESA, E-MOLA, BCI, UNICO, BIM</p>
			</div>
		</div>

      	<table class="table">
			<thead>
				<tr>
					<td>Partida</td>
					<td>Chegada</td>
					<td>Data & Hora</td>
					<td>Cadeira</td>
					<td>Preço</td>
				</tr>
			</thead>
			<tbody>
				@foreach ($results as $result)
					<tr>
						@foreach ($provinces as $province)
							@if ($province->province_id == $result->from)
								<td>{{$province->name}}</td>
							@endif

							 @if ($province->province_id == $result->to)
								<td>{{$province->name}}</td>
							@endif
						@endforeach
						<td>{{$result->departure_date}}</td>
						<td>{{$result->departure_date}}</td>
						<td>{{$result->price}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
    <div class="bg-dark text-white p-5">
		<div class="row">
			<div class="col-md-4">		
			</div>
			<div class="col-md-3 text-right">
				<h6>Sub - Total</h6>
				<h3>{{ $results[0]->price - ($results[0]->price * 0.17)}},00 MT</h3>
			</div>
			<div class="col-md-2 text-right">
				<h6>IVA(17%)</h6>
				<h3>{{$results[0]->price * 0.17}},00 MT</h3>
			</div>
			<div class="col-md-3 text-right">
				<h6>Total</h6>
				<h3>{{ $results[0]->price}} MT</h3>
			</div>
		</div>
    </div>
</div>


{{-- <div class="home-section container">

        <a class="weatherwidget-io" href="https://forecast7.com/pt/n25d9732d57/maputo/" data-label_1="MAPUTO" data-label_2="TEMPERATURA" data-theme="pure" >MAPUTO TEMPERATURA</a>
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

        <div class="row">
            <div class="col-md-12">
                <table class="table tab" >
                    <thead>
                        <tr class="soft-shadow">
                            <th class="py-0 border-0"></th>
                            <th class="py-0 border-0" colspan="2">Origem</th>
                            <th class="py-0 border-0">Destino</th>
                            <th class="py-0 border-0">Data de partida</th>
                            <th class="py-0 border-0">Preço & Hora</th>
                            <th class="py-0 border-0">Referência</th>
                            <th class="py-0 border-0">Operação</th>
                        </tr>
                    </thead>
                    <tbody class="m-4">
                        @foreach ($results as $result)
                        <tr class="soft-shadow">
                                <td class="py-0 border-0 p-2"> <img width="32px" src="/images/route.png" alt="icone"></td>
                                @foreach ($provinces as $province)
                                    @if ($province->province_id == $result->from)
                                        <td class="py-0 border-0">Provincia de <br>{{$province->name}}</td>
                                        <td class="py-0 border-0"><b><i class="lni-arrow-right"></i></b></td>
                                    @endif
                                    @if ($province->province_id == $result->to)
                                        <td class="py-0 border-0">Provincia de <br>{{$province->name}}</td>
                                    @endif
                                @endforeach
                            
                            <td class="py-0 border-0">{{$result->departure_date}} <br> </td>    
                            <td class="py-0 border-0">{{$result->price}} MZN <br> {{$result->state}}</td>
                            <td class="py-0 border-0 pt-2"> {{$result->reference}}</td>
                            <td class="py-0 border-0"> 
                                    <a class="btn btn-sm btn-success mt-2 border-0 text-white" type="submit" href="passagens/reservar">Pagar</a>
                                    <a class="btn btn-sm btn-success mt-2 border-0 text-white" type="submit" href="passagens/reservar">Cancelar</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            <div class="col-md-4">
                
            </div>
        </div>

        <div class="row">
            <col>
        </div>
</div> --}}

<table style="display: none" id="ticket-print" class="ticket-print">
	<tr>
		<th style="background: rgb(231, 76, 60); color: white;">Referencia</th>
		<td>{{$results[0]->reference}}</td>
	</tr>
	<tr>
		<th style="background: rgb(231, 76, 60); color: white;">Data da Viagem</th>
		<td>{{$results[0]->departure_date}}</td>
	</tr>
	<tr>
		<th style="background: rgb(231, 76, 60); color: white;">Origem</th>
		@foreach ($provinces as $province)
			@if ($province->province_id == $results[0]->from)
				<td>{{$province->name}}</td>
			@endif
		@endforeach
	</tr>
	<tr>
		<th style="background: rgb(231, 76, 60); color: white;"><b>Destino</b></th>
		@foreach ($provinces as $province)
			@if ($province->province_id == $results[0]->to)
				<td>{{$province->name}}</td>
			@endif
		@endforeach
	</tr>
	<tr>
		<th style="background: rgb(231, 76, 60); color: white;">Cadeira</th>
		<td>{{$results[0]->seat}}</td>
	</tr>
	<tr>
		<th style="background: rgb(231, 76, 60); color: white;">Preço + (IVA)</th>
		<td>{{$results[0]->price}} MT</td>
	</tr>
	<tr>
		<th style="background: rgb(231, 76, 60); color: white;">Nome</th>
		<td>{{$results[0]->name}} {{$results[0]->surname}}</td>
	</tr>
	<tr>
		<th style="background: rgb(231, 76, 60); color: white;">Contacto</th>
		<td>{{$results[0]->contact}}</td>
	</tr>
	<tr>
		<th style="background: rgb(231, 76, 60); color: white;">Familiar</th>
		<td>{{$results[0]->parent}}</td>
	</tr>
</table>




@stop