@extends('layouts.root')

@section('title', 'ETRAGO')


@section('content')

<style type="text/css">
	body{
		color: #999;
		background: #e2e2e2;
		font-family: 'Roboto', sans-serif;
	}
	.form-control{
		min-height: 41px;
		box-shadow: none;
		border-color: #e1e1e1;
	}
	.form-control:focus{
		border-color: #00cb82;
	}	
    .form-control, .btn{        
        border-radius: 3px;
    }
	.form-header{
		margin: -30px -30px 20px;
		padding: 30px 30px 10px;
		text-align: center;
    	background: linear-gradient(90deg, rgba(74,163,255,1) 0%, rgba(0,85,227,1) 100%) !important;
		border-bottom: 1px solid #eee;
		color: #fff;
	}
	.form-header p{
		font-size: 17px;
		line-height: normal;
		font-style: italic;
		font-family: 'Roboto', sans-serif;
	}
    .signup-form{
		width: 60%;
		margin: 0 auto;	
		padding: 30px 0;	
	}
    .signup-form form{
		color: #999;
		border-radius: 3px;
    	margin-bottom: 15px;
        background: #f0f0f0;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
	.signup-form .form-group{
		margin-bottom: 20px;
	}
    .signup-form .btn{        
        font-size: 16px;
        font-weight: bold;
		background: #212121;

		border: none;
		min-width: 200px;
	}
	input { 
    text-align: center; 
}
</style>


<div class="home-section container">
        <a class="weatherwidget-io" href="https://forecast7.com/pt/n25d9732d57/maputo/" data-label_1="MAPUTO" data-label_2="TEMPERATURA" data-theme="pure" >MAPUTO TEMPERATURA</a>
        <script>
        	!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
        </script>

	<div class="signup-form">
		<form action="{{ action('HomeController@seeReservation')}}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-header">
				<p>Preencha o campo abaixo com a referência atribuida no momento que efectuou a reserva, caso nao tenha efectue uma nova...</p>
			</div>
			<div class="form-group">
				<input type="text" class="form-control" name="reference" required="required" placeholder="Ex: ER45TR">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block btn-lg">Procurar</button>
			</div>	
		</form>
	</div>
</div>
@stop