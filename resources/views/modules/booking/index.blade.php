@extends('adminlte::page')

@section('title', 'Reservas')

@section('content_header')
    <h1>Reservas Efectuadas</h1>
@stop

<link rel="stylesheet" href="../css/table-style.css">

@section('content')
<div class="alert alert-info alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-info"></i> Informação!</h4>
	Esta página exibe todas as reservas efectuadas no sistema
</div>

@if ($message = Session::get('success'))
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i> Alerta </h4>
		{{$message}}
	</div>
@endif

<div class="table-wrapper">
	<div class="table-title">
		<div class="row">
			<div class="col-sm-4">
				<h2><b>Reservas</b> efectudas no sistema</h2>
			</div>
			<div class="col-sm-8">						
				<a href="#" class="btn btn-primary"><i class="material-icons">&#xE863;</i> <span>Actualizar Lista</span></a>
				<a onclick="printBooks()" class="btn btn-info"><i class="material-icons">&#xE24D;</i> <span>Imprimir Lista</span></a>
			</div>
		</div>
	</div>
		
	<table id="dataTable" class="table table-striped table-hover display">
		<thead>
			<tr>
				<th>Referência</th>
				<th>Nome</th>
				<th>Apelido</th>
				<th>Contacto</th>
				<th>Data Partida</th>
				<th>Estado</th>
				<th>Operação</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($reservations as $reservation)
				<tr>
					<td>{{$reservation->reference}}</td>
					<td>{{$reservation->name}}</td>
					<td>{{$reservation->surname}}</td>
					<td>{{$reservation->contact}}</td>
					<td>{{$reservation->departure_date}}</td>
					<td><span class="label label-success">{{$reservation->book_state}}</span></td>
					<td>
						<a href="{{ url('admin/reservas/'.$reservation->reservation_id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
						<a href="{{ url('admin/reservas/'.$reservation->reservation_id.'/editar') }}" class="btn btn-sm btn-warning text-white"><i class="fa fa-pen"></i></a>
						{!! Form::open(['method' => 'DELETE','route' => ['reservas.destroy', $reservation->reservation_id],'style'=>'display:inline']) !!}
							<button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}
					</td>
				</tr>
			@empty
				<tr>
					<td colspan="7" class="text-center">Sem Reservas Efectuadas</td>
				</tr>
			@endforelse
			
			</tbody>
		</table>
</div>



  <table id="table-print" class="table-print" style="display: none">
		<thead>
			<tr style="background: rgb(231, 76, 60); color: white;">
				<th>Referência</th>
				<th>Nome</th>
				<th>Apelido</th>
				<th>Contacto</th>
				<th>Data Partida</th>
				<th>Estado</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($reservations as $reservation)
				<tr>
					<td>{{$reservation->reference}}</td>
					<td>{{$reservation->name}}</td>
					<td>{{$reservation->surname}}</td>
					<td>{{$reservation->contact}}</td>
					<td>{{$reservation->departure_date}}</td>
					<td>{{$reservation->book_state}}</td>
				</tr>
			@empty
				<tr>
					<td colspan="7" class="text-center">Sem Reservas Efectuadas</td>
				</tr>
			@endforelse
		</tbody>
	</table>
@stop