@extends('adminlte::page')

@section('title', 'Bus Booking')

@section('content_header')
    <h1>Administração</h1>
@stop




<style>

.profile-user-img{
	height: 100px !important;
	width: 100px !important;
}

</style>
	
	@section('content')

<div class="row">
		<div class="col-md-3">
			<div class="box box-primary">
				<div class="box-body box-profile">
					<img class="profile-user-img img-responsive img-circle" src="../storage/images/users/{{$user_details->profile_photo}}">
	  
				  <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>
	  
					<p class="text-muted text-center">{{ $user->roles->first()->name }}</p>
	  
					<ul class="list-group list-group-unbordered">
					  <li class="list-group-item">
						<b>Ultimo login</b> <a class="pull-right">{{ $user->last_login_at }}</a>
					  </li>
					  <li class="list-group-item">
						<b>Endereço IP</b> <a class="pull-right">{{ $user->last_login_ip }}</a>
					  </li>
					</ul>
				  </div>
				  <!-- /.box-body -->
				</div>
				<!-- /.box -->
				@if ($user->roles->first()->name ==='Superuser' || $user->roles->first()->name ==='Administrador')
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Ultimos Acessos</h3>
					</div>
					<div class="box-body">
						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>Usuario</b> <spam class="pull-right"><b>Data & Hora</b></span>
							</li>
							@foreach ($users as $item)
								<li class="list-group-item">
									<b>{{ $item->username }}</b> <a class="pull-right">{{ $item->last_login_at }}</a>
								</li>
							@endforeach
							
						</ul>
					</div>
					<!-- /.box-body -->
				</div>
				@endif
				<!-- About Me Box -->
				
				<!-- /.box -->
			  </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="fa fa-pointer"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Destinos</span>
				<span class="info-box-number">{{$routes}}</span>
			</div>
		</div>
	</div>
	

    <div class="col-md-3 col-sm-6 col-xs-12">
      	<div class="info-box">
			<span class="info-box-icon bg-red"><i class="fa fa-car"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Autocarros</span>
				<span class="info-box-number">{{$buses}}</span>
			</div>
      	</div>
    </div>
	
	
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-green"><i class="ion ion-ios-book"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">Reservas</span>
				<span class="info-box-number">{{$books}}</span>
			</div>
		</div>
	</div>
	

	<div class="col-md-9">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Moçambique</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			
			<div class="box-body no-padding">
				<iframe src="https://www.google.com/maps/d/u/0/embed?mid=13Hz3ZIsBGLH_bmoZmev9vO6WDT1NynVD" width="100%" height="100%"></iframe>
			</div>
		</div>
	</div>
</div>
@stop