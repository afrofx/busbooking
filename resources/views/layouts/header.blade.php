<header>
    <div class="container" id="top">
        <div class="row">
            <div class="col-md-10 col-sm-12 col-xs-12">
                <ul class=" list-group list-group-horizontal">
                    <li><i class='fas fa-phone pr-2'></i> +258 84 00 00 8</li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-12 col-xs-12 text-left">
                <i class='fab fa-facebook'></i>
                <i class='fab fa-twitter-square'></i>
                <i class='fab fa-google-plus-square'></i>
            </div>
        </div>
    </div>
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-dark ownbg">
            <a class="navbar-brand text-white pl-5" href="/">
            ETRAGO
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/destinos">Destinos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/minhareserva">Minha Reserva</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/acerca">Acerca</a>
                    </li>     
                </ul>
            </div>  
        </nav>
    </div>
</header>