<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- --------------------------Fontawesome Icons----------------------------- --}}
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/fontawesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/brands.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/solid.css')}}" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/datatables/default/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
</head>
<body>
    @include('layouts.header')
    <div>
        <main>
            @yield('content')
        </main>
    </div>
    @include('layouts.footer')
    {{-- --------------------------Fontawesome Icons----------------------------- --}}
    <script src="{{ asset('assets/fonts/fontawesome/js/brands.js') }}"></script>
    <script src="{{ asset('assets/fonts/fontawesome/js/solid.js') }}"></script>
    <script src="{{ asset('assets/fonts/fontawesome/js/fontawesome.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
    
    <script src="{{ asset('assets/plugins/jspdf/jspdf.debug.js') }}"></script>
    <script src="{{ asset('assets/plugins/jspdf/jspdf.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jspdf/autotable.min.js') }}"></script>
    
    <script src="{{ asset('assets/plugins/datatables/default/js/dataTables.bootstrap4.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/plugins/jspdf/examples-autotable.js') }}"></script> --}}

    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.pt.min.js') }}"></script>

    <script src="{{ asset('js/jquery.seat-charts.js') }}"></script>
    <script src="{{ asset('js/js.js') }}"></script>
</body>
</html>
